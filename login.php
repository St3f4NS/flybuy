<?php
    include(__DIR__ . '/Libraries/core/accop.php');

sec_session_start();
	if (!login_check())
	{
		
		ob_start();
		echo '<br>Login now: <br>';

		if (isset($_GET['error']))
		{
			$m = $_GET['error'];
			$msg;
			switch ($m) 
			{
				case 'invalid':
					$msg = INVALID_LOGIN;
					break;
				case 'mail':
					$msg = MAIL_FORM;
					break;
				case 'fields':
					$msg = FIELDS_INCOMPLETE;
					break;

			}
			echo $msg;
		}
		$root = $_SERVER["DOCUMENT_ROOT"];

		include($root . "/forms/formlogin.html");
		echo 'or <a href="register.php">register</a>!';
		$content = ob_get_clean();
	}
	else
	{
		ob_start();
		echo 'Hello '.$_SESSION['username'].'! How are you today?';
		echo '<br><a href="Libraries/logout_handle.php">Logout</a>';
		$content = ob_get_clean();
	}
	print($content);
?>