<?php
include_once("Libraries/core/accop.php");
include_once("Libraries/paypal_handler.php");

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="Templates/css/compiled.css" />
    <!--<link rel="stylesheet" href="../less/bxslider.less"/>-->
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <title>Flybuy</title>
</head>
<body>

<div class="header-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-2">

                <div class="dropdown dropdown-help">
                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        HELP
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <!--li><a href="#">FAQ</a></li-->
                        <!--li><a href="#">Submit ticket</a></li-->
                    </ul>
                </div>

            </div>
            <div class="col-md-2 col-md-offset-8">
                <!--NOTIFICATIONS HERE-->
            </div>
        </div>
    </div>
</div>
<hr class="top-hr">

<div class="header-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-3 logo">
                <a href="/">
                    <img src="images/logo.png" alt="flybuy logo"/>
                </a>
            </div>

            <div class="col-md-3 col-md-offset-6">
            </div>

        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.header-profile -->




<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default content-container content-container-confirmation-link">
                <div class="panel-heading content-title">
                    <h3 class="panel-title">Activation/Payment</h3>
                </div>
                <div class="panel-body panel-content">
                    <?php if (isset($_GET['id'], $_GET['user'])) {
                        $id = $_GET['id'];
                        $user = $_GET['user'];
                        if (isset($_GET['op'])) {
                            if ($_GET['op'] == 0)
                            {
                                if (verify($user, $id)) { ?>
                                    <p>You have successfully activated your account!</p>
                                    <?php
                                    if ($_GET['type'] != 1) {
                                        $date = setSubscriptionDate(0, $user);
                                        ?>
                                        <p>Your subscription end date: <?php echo $date; ?></p>
                                        <!--p>The Only thing left is to choose your package and finish payment</p>
                                        <p>Please, choose package:</p>
                                        <div class="radio-buttons-registration-link">
                                            <form action="active.php?op=1&user=<?php echo $_GET['user']; ?>" method="get">
                                                <input type="hidden" name="op" value="1">
                                                <input type="hidden" name="id" value="<?php echo $_GET['user']; ?>">

                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="payment_option" id="optionsRadios1"
                                                               value="0" checked>
                                                        Monthly package - £ 30
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="payment_option" id="optionsRadios2"
                                                               value="1">
                                                        Annual package - £ 300 ( £ 60 saving )
                                                    </label>
                                                </div>
                                                <button type="submit" class="btn btn-buy">BUY NOW</button>

                                            </form>
                                        </div-->
                                    <?php }
                                }
                                else
                                { ?><p>Invalid validation link.</p> <?php }
                            }
                        }

                    }

                    if (isset($_GET['op'])) {
                        if ($_GET['op'] == 2) {
                            ?>
                            <p>Please, choose package:</p>
                            <div class="radio-buttons-registration-link">
                                <form action="active.php" method="get">
                                    <input type="hidden" name="op" value="1">
                                    <input type="hidden" name="id" value="<?php echo $_GET['user']; ?>">

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="payment_option" id="optionsRadios1"
                                                   value="0" checked>
                                            Monthly package - £ 30
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="payment_option" id="optionsRadios2"
                                                   value="1">
                                            Annual package - £ 300 ( £ 60 saving )
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-buy">BUY NOW</button>

                                </form>
                            </div>
                            <?php
                        }
                    }
                    if (isset($_GET['op'])) {
                        if ($_GET['op'] == 1) {
                            if (isset($_GET['payment_option'])) {
                                ?>
                                <p>Please proceed by clicking the button below.</p>
                                <?php
                                $paypal = createPayment($_GET['id'], $_GET['payment_option']);
                                $paypal->print_buy_button();
                            }
                        }
                    }
                    if (isset($_GET['status']))
                    {
                        $status = $_GET['status'];
                        $user = $_GET['id'];

                        switch ($status) {
                            case 'cancel':
                                $message = "You have canceled your payment procedure, please bare on mind that none will be able to see your products until you make a payment for subscription.";
                                break;
                            case 'paid':
                                $paypal = createPayment($user, $_GET['period']);
                                $response = $paypal->process_payment();
                                $date = setSubscriptionDate($_GET['period'], $user);
                                $message = "Your payment is now completed. Your subscription expires at: " . $date['data'];
                                break;

                        }
                        echo $message;

                    }
                    ?>
                    </div>
                </div>
            </div>
    </div>
</div>

<footer class="footer">

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3>SOCIAL NETWORKS</h3>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/pages/FlyBuyWorld/1589919101266656">
                            <img src="Templates/images/facebook-footer.png" alt=""/>
                            Like us on Facebook
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/flybuyworld">
                            <img src="Templates/images/twitter-footer.png" alt=""/>
                            Follow us on Twitter
                        </a>
                    </li>
                    <li>
                        <a href="/#!/soon">
                            <img src="Templates/images/instagram-footer.png" alt=""/>
                            Follow us on Instagram
                        </a>
                    </li>
                    <li>
                        <a href="/#!/soon">
                            <img src="Templates/images/linkedin-footer.png" alt=""/>
                            Follow us on LinkedIn
                        </a>
                    </li>
                    <li>
                        <a href="/#!/soon">
                            <img src="Templates/images/google-footer.png" alt=""/>
                            Join us on Google Plus
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>RULES AND POLICIES</h3>
                <ul>
                    <li><a href="/#!/terms">Terms and conditions</a></li>
                    <li><a href="/#!/ipr">IPR policy</a></li>
                    <li><a href="/#!/privacy">Privacy policy</a></li>
                    <li><a href="/#!/product-policy">Product Listing policy</a></li>
                    <li></li>
                    <!--li><a href="#">Making Payment</a></li>
                    <li><a href="#">Reset password</a></li>
                    <li><a href="#">Buyer protection</a></li-->
                </ul>
            </div>
            <div class="col-md-4">
                <h3>CUSTOMERS SERVICE</h3>
                <ul>
                    <li><a href="/#!/register">Create an Account</a></li>
                    <li><a href="/#!/soon">Help Centre</a></li>
                    <li><a href="/#!/soon">Give us your feedback</a></li>
                    <li><a href="/#!/contact">Contact us</a></li>
                </ul>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="footer-copyrights">
                    <p>©2015 FlyBuy. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>

</footer>

<!-- SCRIPTS GOES HERE-->
<script src="Templates/js/jquery-2.1.3.min.js"></script>
<script src="Templates/js/bootstrap.min.js"></script>
<script src="Templates/js/jquery.bxslider.js"></script>
<script src="Templates/js/main.js"></script>
</body>
</html>
