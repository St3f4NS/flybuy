$(document).ready(function(){

    $header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
    $screen_height = $(window).height();
    $slider_height = $screen_height - $header_height;
    // SET HEIGHT OF SLIDER ON LOAD
    $('.slider-homepage').height($slider_height);

    // POSITIONING menu slider
    $menu_slider_pos = $slider_height - $('.search-row').height()  - 140;
    $('.slider-menu').css('margin-top', $menu_slider_pos);

    // FUNCTION TO SET SLIDER HEIGHT ON RESIZING
    $( window ).resize(function() {
        $header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
        $screen_height = $(window).height();
        $slider_height = $screen_height - $header_height;
        // SET HEIGHT OF SLIDER ON RESIZE
        $('.slider-homepage').height($slider_height);

        // SET MARGIN/POSITON of menu slider
        $menu_slider_pos = $slider_height - $('.search-row').height()  - 140;
        $('.slider-menu').css('margin-top', $menu_slider_pos);

        // SERVICES SLIDER HEIGHT
        $('.slider-content-services').height($slider_height);


    }); // END OF window.resize

    // SERVICES SLIDER HEIGHT

    $('.slider-content-services').height($slider_height);

    // SCROLL stuff services



}); // END OF document.ready
