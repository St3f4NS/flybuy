<?php
    include_once(__DIR__ . '/core/accop.php');
    include_once(__DIR__ . '/core/adminop.php');
    sec_session_start();

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request)) {
        if (isset($request->op))
        {
            switch($request->op) {
                case 0:
                    echo json_encode(getPendingValidations());
                    break;
                case 1:
                    echo json_encode(getSelectedValidation($request->company, $request->validation));
                    break;
                case 2:
                    if ($request->answer == 1)
                        setDisapproved($request->company, $request->validation, $request->msg);
                    else
                        setApproved($request->company, $request->validation);
                    return 1;
                    break;
            }
        }

    }
?>