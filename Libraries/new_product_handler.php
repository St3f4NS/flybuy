<?php
	include_once(__DIR__ . '/core/accop.php');
	include_once(__DIR__ . '/core/product.php');
    include_once(__DIR__ . '/core/translate_meta.php');
	sec_session_start();
	$postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->op))
    {
    	if ($request->op == 1)
    	{
    		include_once(__DIR__ . '/categories.php');
    		$req = getFullList();
    		echo json_encode($req);
    	}
    	elseif ($request->op == 2)
    	{
    		if (login_check())
    		{
                $user = $_SESSION['user_id'];
    			$type = getAccountType($user);
    			if ($type == 0 || $type == 2) {
                    $id = newProduct($request->product);
                    echo json_encode(array($id));
                }
    		}
    		else
    			echo json_encode("Please log-in to proceed");
    	}
        elseif ($request->op == 3)
        {
            if (login_check())
            {
                $ret = updateProduct($request->d);
            }
        }
    }


    function newProduct($data)
    {
    	$meta = array();
    	$discounts = array();
    	$pictures = array();
    	$shipping = array();
    	$variations = array();

    		foreach ($data as $key => $value) {
    			switch ($key) {
    				case 'name':
    					$name = $value;
    					break;
    				
    				case 'category':
    					$category = $value;
    					break;
    				
    				case 'discounts':
    					foreach ($value as $k => $v)
    					{
    						$discounts[] = $v;
    					}
    					break;

    				case 'pictures':
    					foreach ($value as $k => $v) 
    					{
    						$pictures[] = $v;
    					}
    					break;

    				case 'shipping':
    					foreach ($value as $k => $v) 
    					{
    						$shipping[] = $v;
    					}
    					break;

    				case 'variations':
    					foreach ($value as $k => $v) 
    					{
    						$variations[] = $v;
    					}
    					break;
    					
    				default:
    					$meta[] = array($key, $value);
    					break;
    			}
    		}

    	$prod = new Product();
        $company = getUserCompany($_SESSION['user_id']);
    	$id = $prod->addProduct($category, $company, $name);
    	$prod->addProductMeta($meta);
    	$prod->addPictures($pictures);
    	$prod->addDiscounts($discounts);
    	$prod->addVariations($variations);
    	$prod->writeSQL();
        echo $id;
        //$head = 'Location: ../#/product/' . $id;
        //header($head);
    }

    function updateProduct($data)
    {
        $data = (array)$data;
        $product_id = $data['id'];
        $name = $data['name'];
        $price = $data['price'];
        $currency = $data['currency'];
        $quantity = $data['quantity'];
        $description = $data['description'];
        $discounts = $data['discounts'];
        $pictures = $data['pictures'];
        $variations = $data['variations'];

        $product = new Product($product_id);
        $old_data = $product->getProduct();

        $update_meta = array();

        if ($price != $old_data['price'])
            $update_meta['price'] = $price;
        if ($currency != $old_data['currency'])
            $update_meta['currency'] = $currency;
        if ($quantity != $old_data['meta'][1]['quantity'])
            $update_meta['quantity'] = $quantity;
        if ($description != $old_data['meta'][0]['description'])
            $update_meta['description'] = $description;

        $imgs = $old_data['pictures'];
        $product->updateMain(array("name" => $name));
        $product->updateMeta($update_meta);
        $add_imgs = array_diff($pictures, $imgs);
        $rem_imgs = array_diff($imgs, $pictures);

        $product->updateDiscounts($discounts);
        $product->updatePicture($add_imgs);
        $product->removeImage($rem_imgs);

        $variation_update = array();

        $variations = (array)$variations;

        foreach ($variations as $variation) {
            $var = (array)$variation;
            if (array_key_exists('id', $var)) {
                $var_id = $var['id'];
                foreach ($var as $key => $value) {
                    if ($key != 'id') {
                        $variation_update[$var_id][$key] = $value;
                    }
                }
            }
            else {
                $temp = array();
                foreach ($var as $key => $value) {
                    $k = metaCrypt($key);
                    $temp[] = array($k => $value);
                }
                $product->updateNewVariation($temp);
            }
        }

        $product->updateVariation($variation_update);
        $product->clearCache();
    }

?>