<?php
    require_once(__DIR__ . '/../Configs/Config.php');
    require_once(__DIR__ . '/phpfastcache.php');
    require_once(__DIR__ . '/core/mysqlconnection.php');
    require_once(__DIR__ . '/paypal/paypal-digital-goods.class.php' );
    require_once(__DIR__ . '/paypal/paypal-purchase.class.php');
    require_once(__DIR__ . '/paypal/paypal-subscription.class.php');

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->user)) {
        $id = $request->user;
        $mysqli = DBConnection::instance()->db();
        $info = $mysqli->where('user_id', $id)
                        ->getOne('subscription_payments', 'data');
        if ($mysqli->count == 0)
            echo json_encode(array(-1));
        else
            echo json_encode(array($info['data']));
    }
    //PayPal_Digital_Goods_Configuration::environment( 'live' );
    PayPal_Digital_Goods_Configuration::username(PAYPAL_USERNAME);
    PayPal_Digital_Goods_Configuration::password(PAYPAL_PASSWORD);
    PayPal_Digital_Goods_Configuration::signature(PAYPAL_SIGNATURE);
    PayPal_Digital_Goods_Configuration::business_name( 'FlyBuy' );

    function createPayment($userId, $period) {
// 0- month, 1-year
        if ($period == 0) {
            $p_text = "month";
            $p_price = '30';
            $description = 'Flybuy subscription for period of 1 ' . $p_text . ' + 1 month gratis, with price of ' . $p_price . 'GBP total, user ID: ' . $userId;
        }
        if ($period == 1) {
            $p_text = "year";
            $p_price = '300';
            $description = 'Flybuy subscription for period of 1 ' . $p_text . ', with price of ' . $p_price . 'GBP total, user ID: ' . $userId;
        }

        $purchase_details = array(
            'name'        => 'FlyBuy',
            'description' => 'FlyBuy subscription',
            'amount'      => $p_price, // Total including tax
            'tax_amount'  => '0', // Just the total tax amount
            'items'       => array(
                array(
                    'item_name'        => 'FlyBuy subscription',
                    'item_description' => $description,
                    'item_amount'      => $p_price,
                    'item_tax'         => '0',
                    'item_quantity'    => 1,
                    'item_number'      => $userId
                )
            )
        );

        PayPal_Digital_Goods_Configuration::return_url( ADDRESS . '/active.php?status=paid&id=' . $userId . '&period=' . $period);
        PayPal_Digital_Goods_Configuration::cancel_url( ADDRESS . '/active.php?status=cancel&id=' . $userId);
        PayPal_Digital_Goods_Configuration::notify_url( ADDRESS . '/active.php?status=notify' );

        PayPal_Digital_Goods_Configuration::currency( 'GBP' );

        $paypal_purchase = new PayPal_Purchase( $purchase_details );
        return $paypal_purchase;
    }

    function setSubscriptionDate($period, $user)
    {
        $db = DBConnection::instance()->db();

        $row = $db->where('user_id', $user)->getOne('subscription_payments', 'data');

        if ($period == 0)
            $p_v = "+1M";
        else
            $p_v = "+1Y";

        $date = $db->now($p_v);

        if ($db->count > 0)
        {
            $data = array('data' => $date);
            $update = $db->where('user_id', $user)
                            ->update('subscription_payments', $data);
        }
        else
        {
            $data = array('user_id' => $user,
                            'data' => $date);
            $insert = $db->insert('subscription_payments', $data);
        }

        $cache = phpFastCache();
        if ($cache->isExisting("user_array_" . $user))
            $cache->delete("user_array_" . $user);

        $new_date = $db->where('user_id', $user)->getOne('subscription_payments', 'data');
        return $new_date['data'];
    }
?>