<?php
    include_once(__DIR__ . "/core/accop.php");
	include_once(__DIR__ . "/core/mysqlconnection.php");

	if (isset($_GET['adminSearch']) && $_GET['adminSearch'] == 1)
	{
		$query = $_GET['query'];
		$type = $_GET['type'];
		if ($type == "user")
		{
			$res = searchUser($query);
			if ($res != -1)
			{?>
				<table class="table">
							  <thead>
								  <tr>
									  <th>ID</th>
									  <th>First name</th>
									  <th>Last name</th>
									  <th>Address</th>
									  <th>Company</th>
									  <th>Phone</th>
									  <th>Country</th>
									  <th>State</th>
									  <th>Account type</th>                                          
								  </tr>
							  </thead>   
							  <tbody>
								<tr>
									<td class="center"><?php echo $res['id']; ?></td>
									<td class="center"><?php echo $res['firstname']; ?></td>
									<td class="center"><?php echo $res['lastname']; ?></td>
									<td class="center"><?php echo $res['address']; ?></td>
									<td class="center"><?php echo $res['company']; ?></td>
									<td class="center"><?php echo $res['phone']; ?></td>
									<td class="center"><?php echo $res['country']; ?></td>
									<td class="center"><?php echo $res['state']; ?></td>     
									<td class="center"><?php echo $res['accType']; ?></td>                             
								</tr>
								                       
							  </tbody>
						 </table> 
						 <?php
			}
			else
				echo 'Not found';
		}
		else if ($type == "company")
		{
			$res = searchCompany($query);
            if ($res == -1)
                echo "Not found";
            else {
                ?>
                <table class="table">
                    <thead><?php

                    foreach ($res as $row) {
                        foreach ($row as $key => $value) {
                            echo "<th>" . $key . "</th>";
                        }
                    }
                    ?>
                    </thead>

                    <tbody>
                    <tr>
                        <?php
                        foreach ($res as $row) {
                            foreach ($row as $key => $value) {
                                echo "<td class='center'>" . $value . "</td>";
                            }
                        }
                        ?>
                    </tr>
                    </tbody>
                </table>
            <?php
            }
		}
	}


	function searchUser($query)
	{
		$mysqli = DBConnection::instance()->db();

        $res = $mysqli->where('email', $query)->getOne('user', array('id', 'accountType'));

        if ($mysqli->count > 0)
        {
            $id = $res['id'];
            $accountType = $res['accountType'];
            $firstname = getUserMeta($id, '[firstname]');
            $lastname = getUserMeta($id, '[lastname]');
            $address = getUserMeta($id, '[address]');
            $company = getUserMeta($id, '[company_name]');
            $phone = getUserMeta($id, '[telephone]');

            $country_id = getUserMeta($id, '[country]');
            $state_id = getUserMeta($id, '[state]');

            $c_query = $mysqli->where('id', $country_id)->getOne('country', 'name');
            $country = $c_query['name'];
            $s_query = $mysqli->where('id', $state_id)->getOne('state', 'name');
            $state = $s_query['name'];

            if ($accountType == 0)
                $aT = 'Supplier';
            else if ($accountType == 1)
                $aT = 'Buyer';
            else
                $aT = 'Both';

            $res = array( "id" => $id, "firstname" => $firstname, "lastname" => $lastname, "address" => $address, "company" => $company, "phone" => $phone, "country" => $country, "state" => $state, "accType" => $aT);
            return $res;
        }
        else
            return -1;


	}

	function searchProduct($query)
	{

	}

	function searchCompany($query)
	{
		$mysqli = DBConnection::instance()->db();

        $c_query = $mysqli->where('data', '[company_name]')->where('value', $query)->getOne('company_data', 'company_id');
        if ($mysqli->count > 0) {
            $id = $c_query['company_id'];

            $get_user = $mysqli->join('company', 'user.id = company.user_id', 'INNER')
                ->where('company.id', $id)
                ->getOne('user', 'email');
            $email = $get_user['email'];

            $get_data = $mysqli->where('company_id', $id)
                ->get('company_data', null, array('data', 'value'));

            $data = array();
            $data[] = array('owner_email' => $email, 'id' => $id);

            foreach ($get_data as $row) {
                $data[] = array($row['data'] => $row['value']);
            }
            return $data;
        }
        else
            return -1;

	}

?>