<?php
    include_once(__DIR__ . '/mail_handler.php');
	include_once(__DIR__ . '/core/accop.php');
	include_once(__DIR__ . '/core/messages.php');
	sec_session_start();

	$postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->o))
    {
        switch($request->o){
            case 0:
                echo getList($request->u_id);
                break;
            case 1:
                echo getConversation($request->c_id, $request->u_id);
                break;
            case 2:
                echo sendMsg($request->c_id, $request->u, $request->t);
                break;
            case 3:
                echo sendNewMsg($request->u2, $request->t, $request->u);
                break;
            case 4:
                echo getNew($request->u);
                break;
            case 5:
                echo getId($request->u, $request->c);
                break;
            case 6:
                echo sendNewMsg($request->u2, $request->t, $request->u, $request->u2_id);
                break;
            case 7:
                sendMail("services@flybuy.world", "FlyBuy", $request->subject, $request->message, $request->mail, $request->name);
                break;
            case 8:
                sendMail("services@flybuy.world", "FlyBuy", "Services", $request->message, $request->replay, $request->name);
                break;
        }
    }


    function getConversation($c_id, $u_id) {
        if ($u_id != $_SESSION['user_id'])
            return json_encode(-1);
        else {
            $messanger = new Messanger($u_id);
            $tree = $messanger->getConversationTree($c_id);
            $messanger->setSeen($c_id);
            return json_encode($tree);
        }
    }

    function getList($u_id) {

        if ($u_id != $_SESSION['user_id'])
            return json_encode(-1);
        else {
            $messanger = new Messanger($u_id);
            $conversations = $messanger->getConversationsList();
            return json_encode($conversations);
        }
    }

    function sendMsg($conversation, $user, $text)
    {

        if ($user != $_SESSION['user_id'])
            return json_encode(-1);
        else {
            $messanger = new Messanger($user);
            $id = $messanger->getPersonInConversation($conversation);
            $msg = $messanger->sendNewMessage($id, $text, $conversation);
            return json_encode($msg);
        }
    }

    function sendNewMsg($u2, $text, $id, $to_id = -1)
    {
            if ($id != $_SESSION['user_id'])
                return json_encode(-1);
            else {
                $to = $u2;
                $messanger = new Messanger($id);
                if ($to_id == -1)
                    $to_id = getUserId($to);

                $con_id = $messanger->checkConversation($to_id);
                if ($to_id != -1) {
                    $msg = $messanger->sendNewMessage($to_id, $text, $con_id);
                    if ($msg == -1)
                        return json_encode("Error while sending message.");
                    else
                        return json_encode($msg);
                } else
                    return "Unable to find specified user.";
            }
    }



    function getId($u, $c)
    {
            if ($u != $_SESSION['user_id'])
                return json_encode(-1);
            else {
                $messanger = new Messanger($u);
                $another_id = $messanger->getPersonInConversation($c);
                return json_encode($another_id);
            }
    }


    function getNew($u)
    {
        if ($u != $_SESSION['user_id'])
            return json_encode(-1);
        else {
            $messanger = new Messanger($u);
            $val = $messanger->getNewMessages();
            return json_encode($val);
        }
    }

?>