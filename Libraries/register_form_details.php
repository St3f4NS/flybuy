<?php
	require_once(__DIR__ . '/core/accop.php');
	require_once(__DIR__ . '/core/vatValidation.php');
	require_once(__DIR__ . '/../Languages/index.php');

	$postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->country, $request->vat))
    {
    	$country = $request->country;
    	$vat = $request->vat;

		$vatValidation = new vatValidation(array('debug' => false));
		
		if($vatValidation->check($country, $vat)) 
			echo json_encode(1);
		else
			echo json_encode(0);
	}

?>