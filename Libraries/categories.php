<?php
include_once('phpfastcache.php');
include_once(__DIR__ . '/core/mysqlconnection.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if (isset($request->op_type))
{
    $op = $request->op_type;
    if ($op == 1) //add value
    {
        if (isset($request->category_id, $request->data, $request->value))
        {
            $var = addValue($request->category_id, $request->data, $request->value);
            echo json_encode($var);
        }
    }
    elseif ($op == 2)
    {
        if (isset($request->category_id))
        {
            $var = getValue($request->category_id);
            echo json_encode($var);
        }
    }
    elseif($op == 3)
    {
        if (isset($request->category_id, $request->data))
        {
            $var = removeData($request->category_id, $request->data);
            echo json_encode($var);
        }
    }
    elseif($op == 4)
    {
        $var = getCategoriesArray();
        echo json_encode($var);
    }
    elseif($op == 5)
    {
        $var = getChilds($request->parent_id);
        echo json_encode($var);
    }
    elseif($op == 6)
    {
        $var = getFullList();
        echo json_encode($var);
    }

}

if (isset($_POST['operation']))
{
    $op = $_POST['operation'];

    switch ($op)
    {
        case 'remove':
            if (isset($_POST['id']))
                removeCategory($_POST['id']);
            break;
        case 'add':
            addCategory($_POST['selectParent'], $_POST['name']);
            header("Location: ../FB-Admin/index.php?page=categories");
            break;
        default:
            # code...
            break;
    }
}

if (isset($_POST['newname']))
{
    $name = $_POST['newname'];
    $id = $_POST['modal_id'];
    $res = getCategoryParams($id);
    $newParent = $_POST['updateParent'];

    if (!empty($name))
    {
        if ($res != $name)
            updateName($id, $name);
    }
    if ($res[1] != $newParent)
        updateParent($id, $newParent);

    header("Location: ../FB-Admin/index.php?page=categories");
}


function getCategoryParams($id)
{
    $mysqli = DBConnection::instance()->db();

    $param = $mysqli->where("id", $id)->getOne("categories", array("parent_id", "name"));

}

function updateName($id, $data)
{
    $mysqli = DBConnection::instance()->db();

    $mysqli->where("id", $id)->update("categories", array("name" => $data));
}

function updateParent($id, $parent_id)
{
    $mysqli = DBConnection::instance()->db();

    $mysqli->where("id", $id)->update("categories", array("parent_id" => $parent_id));
}

function getFullList()
{
    $mysqli = DBConnection::instance()->db();

    $categories = $mysqli->get("categories", null, array("id", "parent_id", "name"));
    $data = array();

    foreach ($categories as $cat) {
        $data[$cat['id']] = array('id' => $cat['id'], 'parent_id' => $cat['parent_id'], 'name' => $cat['name']);
    }

    return $data;

}
function getCategoriesArray()
{
    $mysqli = DBConnection::instance()->db();

    $categories = $mysqli->get("categories", null, array("id", "parent_id", "name"));
    $data = array();

    foreach($categories as $cat) {
        if ($cat['parent_id'] == 1)
            $data[$cat['id']] = array('id' => $cat['id'], 'parent_id' => $cat['parent_id'], 'name' => $cat['name']);
    }

    return $data;
}

function getChilds($id)
{
    $cache = phpFastCache();
    $cat_cache = $cache->get("category_child_" . $id);

    if ($cat_cache == null) {
        $mysqli = DBConnection::instance()->db();
        $categories = $mysqli->where("parent_id", $id)->get("categories", null, array("id", "name"));
        $cache->set("category_child_" . $id, $categories, 3600*24);
    }
    else
        $categories = $cat_cache;
    return $categories;
}

function removeCategory($id)
{
    $mysqli = DBConnection::instance()->db();

    $mysqli->where("id", $id)->orWhere("parent_id", $id)->delete("categories");
}

function addCategory($parent_id, $name)
{
    $mysqli = DBConnection::instance()->db();

    $data = array("parent_id" => $parent_id, "name" => $name);
    $id = $mysqli->insert("categories", $data);

    return $id;
}

function addValue($category_id, $data, $value)
{
    $mysqli = DBConnection::instance()->db();

    $data = array("category_id" => $category_id,
        "data" => $data,
        "value" => $value);
    $ins = $mysqli->insert("category_data", $data);

    $cache = phpFastCache();
    $category_params = $cache->get("category_params_" . $category_id);
    if ($category_params != null)
        $cache->delete("category_params_" . $category_id);

    if ($ins)
        return 1;
    else
        return -1;
}

function getValue($category_id)
{
    $cache = phpFastCache();
    $category_params = $cache->get("category_params_" . $category_id);

    if ($category_params == null)
    {
        $mysqli = DBConnection::instance()->db();

        $val = $mysqli->where("category_id", $category_id)->get("category_data", null, array("data", "value"));

        $data = array();

        foreach($val as $row)
        {
            $data[] = array('key' => $row['data'], 'val' => $row['value']);
        }


        $cache->set("category_params_" . $category_id, $data, 600);
        return $data;

    }
    else
        return $category_params;
}

function removeData($category_id, $data)
{
    $mysqli = DBConnection::instance()->db();

    $mysqli->where("category_id", $category_id);
    $mysqli->where("data", $data);
    $mysqli->delete("category_data");

    $cache = phpFastCache();
    $category_params = $cache->get("category_params_" . $category_id);
    if ($category_params != null)
        $cache->delete("category_params_" . $category_id);

    return 1;
}

?>