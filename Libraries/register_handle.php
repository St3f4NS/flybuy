<?php
    include_once("image_handler.php");
    include_once("mail_handler.php");
	include_once(__DIR__ . '/core/accop.php');

	if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		header("Location: ../#!/register/mail");
	else
	{
		$username = $_POST['username'];
	    $email = $_POST['email'];
	    $password = $_POST['p'];
	    $country = $_POST['country'];
	    $postCode = $_POST['postCode'];
        $uCity = $_POST['uCity'];
	    $address = $_POST['address'];
		$accType = $_POST['accType'];
		$fName = $_POST['fName'];
		$lName = $_POST['lName'];
		$companyName = $_POST['companyName'];
		$phone = $_POST['phone'];
		

		if (preg_match_all('~^' . "number:(?'id'\d+)" . '$~i', $country, $match))
			$country = $match[1][0];

		switch ($accType) 
		{
			case 0:
			case 2:
				$ex = checkCompany($companyName);

				if ($ex == 1)
				{
					$reg = register($username, $email, $password, $accType);
				
					switch ($reg) 
					{
						case ($reg > 0):
							$data = array( '[country]' => $country,
											'[post_code]' => $postCode,
                                            '[ucity]' => $uCity,
											'[address]' => $address,
											'[firstname]' => $fName,
											'[lastname]' => $lName,
											'[telephone]' => $phone);


							addArrayUserMeta($reg, $data);

							$c_id = registerCompany($reg);

							if (isset($_POST['enable_validation']))
							{
                                $val_method = $_POST['v_method'];
                                if ($val_method == 0)
                                {
                                    addPendingValidation($reg, $val_method);

                                    $vatC = $_POST['vatC'];
                                    $cPhone = $_POST['cPhone'];
                                    $cAddress = $_POST['cAddress'];
                                    $cCity = $_POST['cCity'];
                                    $vat = $_POST['vat'];
                                    validationNew($email);
                                    $company_data = array('[company_name]' => $companyName,
                                        '[company_phone]' => $cPhone,
                                        '[company_address]' => $cAddress,
                                        '[company_city]' => $cCity,
                                        '[company_country]' => $vatC,
                                        '[vat_code]' => $vat,
                                        '[active_status]' => 2);
                                }
                                else
                                {
                                    $pending_id = addPendingValidation($c_id, $val_method);
                                    $temp = explode("+", $_POST['validation_imgs']);

                                    foreach($temp as $img) {
                                        if ($img != "") {
                                            $new_img = str_replace("temp","val_img", $img);
                                            renameImage($img, $new_img);
                                            addValidationData($pending_id, $new_img);
                                        }
                                    }
                                    validationNew($email);
                                    $vatC = $_POST['vatC'];
                                    $cPhone = $_POST['cPhone'];
                                    $cAddress = $_POST['cAddress'];
                                    $cCity = $_POST['cCity'];

                                    $company_data = array('[company_name]' => $companyName,
                                        '[company_phone]' => $cPhone,
                                        '[company_address]' => $cAddress,
                                        '[company_city]' => $cCity,
                                        '[company_country]' => $vatC,
                                        '[active_status]' => 2);
                                }
							}
							else
								$company_data = array( '[company_name]' => $companyName,
														'[active_status]' => 0 );

							addCompanyMetaArray($c_id, $company_data);
                            verifyAccount($email, $fName, $reg, $accType);
                            uploadProfileDefault($reg);
							header("Location: ../#!/");
							break;

						case -1:
							header("Location: ../#!/register/exist");
							break;

						default:
							header("Location: ../#!/register/failed");
							break;
					}
				}
				else
					header("Location: ../#!/register/company_exist");
				break;
			
			case 1:
				$ex = checkCompany($companyName);
				if ($ex == 1)
				{
					$reg = register($username, $email, $password, $accType);
					switch ($reg) 
					{
						case ($reg > 0):
							$data = array( '[country]' => $country,
											'[post_code]' => $postCode,
                                            '[ucity]' => $uCity,
											'[address]' => $address,
											'[firstname]' => $fName,
											'[lastname]' => $lName,
											'[telephone]' => $phone);
							
							$company_data = array( '[company_name]' => $companyName );


							addArrayUserMeta($reg, $data);
							$c_id = registerCompany($reg);
							addCompanyMetaArray($c_id, $company_data);
                            verifyAccount($email, $fName, $reg, $accType);
                            uploadProfileDefault($reg);
							header("Location: ../#!/");
							break;

						case -1:
							header("Location: ../#!/register/exist");
							break;

						default:
							header("Location: ../#!/register/failed");
							break;
					}
				}
				break;
		}
		
	}
?>