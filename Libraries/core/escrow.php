<?php

	class Transpact
	{
		protected $tUsername;
		protected $tPassword;
		protected $WSDL = "https://www.transpact.com/SecurePartner/Partner.asmx?WSDL";
		protected $client;
		protected $test = true;

		function __construct()
		{
			if(!class_exists('SoapClient')) 
				throw new Exception('The Soap library has to be installed and enabled');

			$this->client = new SoapClient($this->WSDL, array());

			$this->tUsername = 'simon-27@live.co.uk';
			$this->tPassword = 'testtest';
 
		}

		/*amount - any
        nature - other
        sender fee = 0
        rec fee = 0
        originator = 5.98 gbp
        name  - Fly
        email - Fly
        nominated ref = false
        max days - 14
        conditions - ?
        conditions consumer clause - false
        can trans change - false;
        originat pcnt commision on receive 2.5
        money sender mon - sender
        recipient mon - receiver*/

		function newTransaction($senderEmail, $receiverEmail, $amount, $currency)
		{
            switch ($currency) {
                case 'USD':
                    $fee = 19.98;
                    break;
                case 'GBP':
                    $fee = 5.98;
                    break;
                case 'EUR':
                    $fee = 6.98;
                    break;
            }
            $collected = $amount * (3/100);

            if ($collected < $fee)
                $fixed_commision = $fee - $collected;
            else
                $fixed_commision = 0;

			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'CreateType' => 3,
							'MoneySenderEmail' => $senderEmail,
							'MoneyRecipientEmail' => $receiverEmail,
							'Amount' => $amount,
							'Currency' => $currency,
							'NatureOfTransaction' => 5,
							'SenderFee' => 0,
							'RecipientFee' => 0,
							'OriginatorFee' => $fee,
							'NameReferee' => 'Fly Buy',
							'EmailReferee' => 'simon-27@live.co.uk',
							'TranspactNominatedReferee' => false,
							'MaxDaysDisputePayWait' => 14,
							'Conditions' => 'Buying a product',
							'ConditionsConsumerClause' => false,
							'CanTransactorsChangeConditions' => false,
                            'OriginatorFixedCommisionOnReceive' => $fixed_commision,
							'OriginatorPcntCommisionOnReceive' => 3,
                            'MoneyRecipientMoniker' => 'FlyBuy',
							'IsTest' => $this->test);

			$rq = $this->client->CreateTranspact($request);

			return $rq;
		}

		function subscriptionPayment($senderEmail)  //34283
		{
			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'CreateType' => 2,
							'MoneySenderEmail' => $senderEmail,
							'Amount' => 35.98,
							'Currency' => 'GBP',
							'NatureOfTransaction' => 5,
							'SenderFee' => 5.98,
							'RecipientFee' => 0,
							'OriginatorFee' => 0,
							'NameReferee' => 'Fly Buy',
							'EmailReferee' => 'simon-27@live.co.uk',
							'TranspactNominatedReferee' => false,
							'MaxDaysDisputePayWait' => 14,
							'Conditions' => 'Acc validation',
							'ConditionsConsumerClause' => false,
							'CanTransactorsChangeConditions' => true,
							'IsTest' => $this->test,
							'MoneyRecipientMoniker' => 'FlyBuy');

			$rq = $this->client->CreateTranspact($request);

			return $rq;
		}

		function viewTransaction($id)
		{
			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'TranspactNumber' => $id,
							'IsTest' => $this->test);
			$rq = $this->client->ViewTranspact($request);

			return $rq;
		}

		function viewPayeeTransactions($clientMail)
		{
			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'IsTest' => $this->test,
							'ClientEmail' => $clientMail);

			$rq = $this->client->ViewPayeeTranspacts($request);

			return $rq;
		}

		function viewPayerTransactions($clientMail)
		{
			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'IsTest' => $this->test,
							'ClientEmail' => $clientMail);

			$rq = $this->client->ViewPayerTranspacts($request);

			return $rq;
		}

		function viewHistory($id)
		{
			$request = array('Username' => $this->tUsername,
							'Password' => $this->tPassword,
							'TranspactNumber' => $id,
							'IsTest' => $this->test);

			$rq = $this->client->TranspactHistory($request);

			return $rq;
		}

	}

?>