<?php
    require_once("MysqliDb.php");
	require_once(__DIR__ . '/../../Configs/Config.php');

	
	class DBConnection
	{
		protected static $singleton;
		public static $db;

		private function __construct()
		{
            self::$db = new MysqliDb(SERVER, USERNAME, PASS, DB);
		}

		public static function instance() 
		{
	    	if(!(self::$singleton instanceof self)) 
	        	self::$singleton = new self();
    		return self::$singleton;
  		}

  		public static function db()
  		{
  			return self::$db;
  		}


	}
?>