<?php

include_once('mysqlconnection.php');

class ProductFeedback
{
    protected $user_id;
    protected $product_id;
    protected $raiting;
    protected $count;

    function __construct($product_id = -1, $user_id = -1)
    {
        if ($product_id != -1)
            $this->product_id = $product_id;
        if ($user_id != -1)
            $this->user_id = $user_id;
    }

    function getFeedbacks($multiplier)
    {
        $mysqli = DBConnection::instance()->db();

        $amount = 5;
        $from_am = $amount * $multiplier;

        $feedbacks = $mysqli->withTotalCount()
                            ->where('product_id', $this->product_id)
                            ->orderBy('fb_id', 'desc')
                            ->get('feedback_product', array($from_am, $amount), array('fb_id', 'user_id', 'message', 'rate', 'date'));
        $count = $mysqli->totalCount;

        $data = array();
        foreach ($feedbacks as $feedback) {
            $firstname = getUserMeta($feedback['user_id'], '[firstname]');
            $lastname = getUserMeta($feedback['user_id'], '[lastname]');
            $img_ver = getUserMeta($feedback['user_id'], '[img_ver]');

            $feedback['rater_firstname'] = $firstname;
            $feedback['rater_lastname'] = $lastname;
            $feedback['img_ver'] = $img_ver;
            $data[] = $feedback;
        }

        return array($count, $data);
    }

    function getFeedbackCount()
    {
        $mysqli = DBConnection::instance()->db();

        $count = $mysqli->where('product_id', $this->product_id)
                        ->getOne('feedback_product', 'COUNT(fb_id) AS num');
        $this->count = $count['num'];
        return $this->count;
    }

    function getRaiting()
    {
        $mysqli = DBConnection::instance()->db();

        $av = $mysqli->where('product_id', $this->product_id)
                    ->getOne('feedback_product', 'AVG(rate) AS r');

        $this->raiting = floor($av['r'] * 2) / 2;
        return $this->raiting;
    }

    function addFeedback($message, $rate)
    {
        if (!$this->checkIfAlreadyRated()) {
            $mysqli = DBConnection::instance()->db();

            $import = array(
                'product_id' => $this->product_id,
                'user_id' => $this->user_id,
                'message' => $message,
                'rate' => $rate,
                'date' => $mysqli->now()
            );
            $id = $mysqli->insert('feedback_product', $import);

            $data = array();
            $data['fb_id'] = $id;
            $data['user_id'] = $this->user_id;
            $data['message'] = $message;
            $data['rate'] = $rate;
            $data['date'] = date('Y-m-d h:m:s');
            $firstname = getUserMeta($this->user_id, '[firstname]');
            $lastname = getUserMeta($this->user_id, '[lastname]');
            $img_ver = getUserMeta($this->user_id, '[img_ver]');

            $data['rater_firstname'] = $firstname;
            $data['rater_lastname'] = $lastname;
            $data['img_ver'] = $img_ver;
            return array($data);
        }
        else
            return -1;
    }

    function checkIfAlreadyRated()
    {
        $mysqli = DBConnection::instance()->db();
        $res = $mysqli->where('product_id', $this->product_id)
            ->where('user_id', $this->user_id)
            ->getOne('feedback_product', 'fb_id');

        if ($mysqli->count > 0)
            return true;
        else
            return false;
    }

}