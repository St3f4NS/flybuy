<?php
	include_once('mysqlconnection.php');
	include_once('product.php');
	include_once(__DIR__ . '/../phpfastcache.php');

	/**
	* 
	*/
	class ProductList
	{
		protected $category;
		protected $products;
        protected $prod_count;

		function __construct($cat = 0)
		{
			if ($cat != 0)
				$this->category = $cat;
		}

        public static function userProducts($company_id, $ammount = 10, $multiplier = 0)
        {
            $cache = phpFastCache();
            $list_cache = $cache->get("company_products_" . $company_id);

            if ($list_cache == null)
            {
                $mysqli = DBConnection::instance()->db();

                $start = $ammount * $multiplier;

                $product_ids = $mysqli->withTotalCount()
                                        ->where('company_id', $company_id)
                                        ->orderBy('id', 'DESC')
                                        ->get('products', array($start, $ammount), 'id');

                $count = $mysqli->totalCount;
                $products = array();

                if ($count > 0)
                    foreach($product_ids as $ele) {
                        $prod = $ele['id'];
                        $p = new Product($prod);
                        $temp = $p->getProduct();
                        if ($temp == null) {
                            $count--;
                            continue;
                        }
                        else
                            $products[] = $temp;

                    }
                else
                    $products = null;

                $temp = array($count, $products);

                $cache->set("company_products_" . $company_id, $temp, 30);

                return $temp;
            }
            else
                return $list_cache;
        }

		function searchList($query, $page = 0)
		{
            $parts = null;

			if(preg_match('/^(["\']).*\1$/m', $query)) {
                $q = substr($query, 1, -1);
            }
            elseif (strpos($query, " ") === false)
                $q = '%' . $query . '%';
			else {
                $parts = explode(" ", $query);
                $q = '%' . $query . '%';

            }

			$cache = phpFastCache();
			$search_cache = $cache->get("search_" . $q . "_" . $page);

			if ($search_cache == null)
			{
				$mysqli = DBConnection::instance()->db();

                if ($parts) {
                    $mysqli->where('name', array('LIKE' => '%' . $parts[0] . '%'));
                    $i = 0;

                    foreach ($parts as $part) {
                        if ($i == 0)
                        {
                            $i++;
                            continue;
                        }

                        $mysqli->orWhere('name', array('LIKE' => '%' . $part . '%'));
                    }
                }
                else
                    $mysqli->where('name', array('LIKE' => '%' . $q . '%'));

                $mysqli->orderBy('id', 'desc');
                $list = $mysqli->get("products", null, "id");
                $count = $mysqli->count;

                $i = 0;
                foreach($list as $element) {
                    if ($i < $page * 10)
                    {
                        $i++;
                        continue;
                    }
                    elseif($i >= ($page+1) * 10)
                        break;


                    $prod = $element['id'];
                    $p = new Product($prod);
                    $temp = $p->getProduct();

                    if ($p->subscribed != false)
                    {
                        $this->products[] = $temp;
                        $i++;
                    }
                    else {
                        $count--;
                        continue;
                    }

                }
                $ret = array($count, $this->products);
				$cache->set("search_" . $q . "_" . $page, $ret, 60);

				return array($count, $this->products);

			}
			else
				return $search_cache;
		}

		/*
		*	$m - multiplier, $ppp - products per page
		*/
		function getCategoryProducts($m = 0, $ppp = 10)
		{
			$cache = phpFastCache();
			$list_cache = $cache->get("category_products_" . $this->category . "_" . $m . "_" . $ppp);

			if ($list_cache == null)
			{
				$mysqli = DBConnection::instance()->db();
				$next = $m * $ppp;

                $date = date('Y-m-d');

                $count = $mysqli->join('subscription_payments s', 'p.company_id=s.user_id', 'INNER')
                        ->where('p.category_id', $this->category)
                        ->where('s.data', $date, '>')
                        ->getOne('products p', array('COUNT(p.id) AS total', 'p.company_id'));

                $this->prod_count = $count['total'];

                $list = $mysqli->withTotalCount()->rawQuery("SELECT p.id, p.company_id FROM flybuy.products p
                                        INNER JOIN flybuy.subscription_payments s ON
                                              p.company_id = s.user_id AND
                                              s.data > CURDATE()
                                        WHERE p.category_id = ?
                                        ORDER BY p.id DESC
                                        LIMIT ?, ?", array($this->category, $next, $ppp));

                if ($mysqli->count > 0)
                    foreach($list as $ele) {
                        $prod = $ele['id'];
                        $p = new Product($prod);
                        $this->products[] = $p->getProduct();
                        if (!$p->subscribed)
                            $this->prod_count--;
                    }
                else
                    $this->products = null;

                $temp = Array($this->prod_count, $this->products);

				$cache->set("category_products_" . $this->category . "_" . $m . "_" . $ppp, $temp, 200);

				return $temp;

			}
			else
				return $list_cache;


		}

		function getLatest($n)
		{
			$cache = phpFastCache();
			$list_cache = $cache->get("latest_products");

			if ($list_cache == null)
			{
				$mysqli = DBConnection::instance()->db();

                $list = $mysqli->rawQuery("SELECT p.id, p.company_id FROM flybuy.products p
                                        INNER JOIN flybuy.subscription_payments s ON
                                              p.company_id = s.user_id
                                        WHERE s.data > CURDATE()
                                        ORDER BY p.id DESC
                                        LIMIT ?", array($n));

                foreach ($list as $ele) {
                    $prod = $ele['id'];
                    $p = new Product($prod);
                    $temp = $p->getProduct();

                    $this->products[] = $temp;
                }

				$cache->set("latest_products", $this->products, 60);

			}
			else
				$this->products = $list_cache;

			return $this->products;
		}


		function getMostSelling($count = 5)
		{
			$cache = phpFastCache();
			$list_cache = $cache->get("most_selling");

			if ($list_cache == null)
			{
				$mysqli = DBConnection::instance()->db();

                $list = $mysqli->rawQuery("SELECT p.id, p.company_id, p.sold FROM flybuy.products p
                                        INNER JOIN flybuy.subscription_payments s ON
                                              p.company_id = s.user_id
                                        WHERE s.data > CURDATE()
                                        ORDER BY p.sold DESC
                                        LIMIT ?", array($count));

                foreach ($list as $ele) {
                    $prod = $ele['id'];
                    $p = new Product($prod);
                    $this->products[] = $p->getProduct();
                }

				$cache->set("most_selling", $this->products, 600);

			}
			else
				$this->products = $list_cache;

			return $this->products;
		}

        public static function getSidebar()
        {
            $cache = phpFastCache();
            $list_cache = $cache->get("sidebar");
            $sidebar = array();

            if ($list_cache == null)
            {
                $mysqli = DBConnection::instance()->db();

                $list = $mysqli->get('sidebar', null, array('product_id', 'text'));

                foreach ($list as $ele) {
                    $prod = $ele['product_id'];
                    $p = new Product($prod);
                    $sidebar[] = array($p->getProduct(), $ele['text']);
                }

                $cache->set("sidebar", $sidebar, 86400);
            }
            else
                $sidebar = $list_cache;

            return $sidebar;
        }
	}
?>