<?php
    include_once('accop.php');
	include_once('mysqlconnection.php');

	class Messanger
	{
		protected $user_id;

		function __construct($id)
		{	
			$this->user_id = $id;
		}

		function getConversationsList()
		{
			$mysqli = DBConnection::instance()->db();

            $m = $mysqli->rawQuery("SELECT m.id, m.conversation_id, m.sender, m.text, m.seen, m.date, c.user1, c.user2 FROM messages AS m
            INNER JOIN conversations AS c
              ON m.conversation_id = c.id
            INNER JOIN (SELECT MAX(id) as id, conversation_id FROM messages GROUP BY conversation_id) AS m1
              ON m1.id = m.id
            WHERE ? IN (c.user1, c.user2) ORDER BY m.id DESC", array($this->user_id));



			$data = array();

            foreach($m as $conversation)
            {
                if ($conversation['user1'] == $this->user_id)
                    $destination = $conversation['user2'];
                else
                    $destination = $conversation['user1'];

                if ($this->user_id == $conversation['sender'])
                    $seen = 0;
                else
                    $seen = $conversation['seen'];

                $name = getUserMeta($destination, '[firstname]');
                $img_ver = getUserMeta($destination, '[img_ver]');
                $data[] = array('id' => $conversation['conversation_id'], 'user2_id' => $destination, 'img_ver' => $img_ver, 'name' => $name, 'message' => $conversation['text'], 'date' => $conversation['date'], 'seen' => $seen);

            }

			return $data;
		}

		function getNewMessages()
		{
			$mysqli = DBConnection::instance()->db();

            $m = $mysqli->rawQuery("SELECT DISTINCT m.conversation_id FROM messages AS m
			INNER JOIN conversations AS c
			  ON m.conversation_id = c.id
			INNER JOIN (SELECT id FROM messages WHERE sender != ? AND seen = 1) AS m1
			  ON m1.id = m.id
			WHERE ? in (c.user1, c.user2)", array($this->user_id, $this->user_id));

            $data = array();
            foreach ($m as $conv)
            {
                $data[] = $conv['conversation_id'];
            }
            return $data;
		}


		function checkConversation($id)
		{
			$mysqli = DBConnection::instance()->db();

            $conv = $mysqli->where("(user1 = ? AND user2 = ?)", array($this->user_id, $id))
                            ->orWhere("(user1 = ? AND user2 = ?)", array($id, $this->user_id))
                            ->getOne("conversations", "id");
            if ($mysqli->count == 1)
                return $conv['id'];
            else
                return -2;
		}

		function sendNewMessage($id, $text, $conversation)
		{
			$mysqli = DBConnection::instance()->db();
			$c_id = 0;

			if ($conversation < 0)
			{
                $data = array("user1" => $this->user_id, "user2" => $id);
                $c_id = $mysqli->insert("conversations", $data);
			}
			else
				$c_id = $conversation;
            $today = date('Y-m-d');
            $data = array("conversation_id" => $c_id,
                            "sender" => $this->user_id,
                            "text" => $text,
                            "date" => $mysqli->now());

            $msg_id = $mysqli->insert("messages", $data);

			$my_name = getUserMeta($this->user_id, '[firstname]');
            $img_ver = getUserMeta($this->user_id, '[img_ver]');
			$r = array('id' => $msg_id, 'conversation_id' => $c_id, 'img_ver' => $img_ver, 'sender' => $my_name, 'msg' => $text, 'date' => $today);

            return $r;
		}

		function getPersonInConversation($id)
		{
			$mysqli = DBConnection::instance()->db();

            $users = $mysqli->where("id", $id)->getOne("conversations", array("user1", "user2"));

            if ($users['user1'] == $this->user_id)
                return $users['user2'];
            else
                return $users['user1'];
		}

		function setSeen($conversation_id)
		{
			$mysqli = DBConnection::instance()->db();

            $mysqli->rawQuery("UPDATE messages SET seen = 0 WHERE conversation_id = ? AND sender != ?", array($conversation_id, $this->user_id));

		}

		function getConversationTree($id)
		{
			$mysqli = DBConnection::instance()->db();

            $users = $mysqli->where("id", $id)->getOne("conversations", array("user1", "user2"));

			if ($users['user1'] == $this->user_id)
				$with = $users['user2'];
			elseif ($users['user2'] == $this->user_id)
				$with = $users['user1'];
			else
				return;

			$with_name = getUserMeta($with, '[firstname]');
			$my_name = getUserMeta($this->user_id, '[firstname]');
            $my_img_ver = getUserMeta($this->user_id, '[img_ver]');
            $with_img_ver = getUserMeta($with, '[img_ver]');


            $conversations = $mysqli->where("conversation_id", $id)
                ->orderBy("id", "DESC")
                ->get("messages", 30, array("id", "sender", "text", "date"));


            $data = array();

			foreach($conversations as $c)
			{
				if ($c['sender'] == $this->user_id)
					$data[] = array('conversation_id' => $id, 'user2_id' => $with, 'id' => $c['id'], 'img_ver' => $my_img_ver, 'sender' => $my_name, 'msg' => $c['text'], 'date' => $c['date']);
				else
					$data[] = array('conversation_id' => $id, 'user2_id' => $with, 'id' => $c['id'], 'img_ver' => $with_img_ver, 'sender' => $with_name, 'msg' => $c['text'], 'date' => $c['date']);
			}

			return $data;

		}


	}
?>