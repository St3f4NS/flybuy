<?php
	include_once("mysqlconnection.php");
    include_once("payments.php");
    include_once("user_feedback.php");
    include_once(__DIR__ . "/../mail_handler.php");
    include_once(__DIR__ . '/../phpfastcache.php');


	/*********************************************************************************************
	*	USER FUNCTIONS
	**********************************************************************************************/
	
	function sec_session_start()
	{
		$session_name = 'sec_session_start';
		$secure = false;
		$httponly = true;

		ini_set('session.use_only_cookies', 1);
		$cookie_param = session_get_cookie_params();
		session_set_cookie_params($cookie_param["lifetime"], $cookie_param["path"], $cookie_param["domain"], $secure, $httponly);
		session_name($session_name);
		session_start();
		session_regenerate_id();
	}

	function getUserDataArray($user_id)
	{
        $data = array();
        $cache = phpFastCache();
        $user = $cache->get("user_array_" . $user_id);

        if ($user == null) {
            $feedback = new UserFeedback($user_id);
            $rate = $feedback->getRaiting();
            $payment_methods = PaymentMethods::getPaymentMethods($user_id);

            $data = getAllUserMeta($user_id);
            $data['[id]'] = $user_id;
            $temp = getAccountType($user_id);
            $data['[email]'] = $temp['email'];
            $data['[accType]'] = $temp['accountType'];
            $data['[privs]'] = $temp['privs'];

            $date = new DateTime($temp['join_date']);
            $now = new DateTime();
            $interval = $now->diff($date);

            $data['[payment_methods]'] = $payment_methods;

            $data['[join_years]'] = $interval->y;
            $data['[rate]'] = $rate;
            $c = $data['[country]'];
            $data['[country_id]'] = $c;
            $data['[country]'] = getCountryById($c);

            $company_id = getUserCompany($user_id);
            $data['[company_id]'] = $company_id;
            $comp = getAllCompanyMeta($company_id);
            $data['[company_name]'] = $comp['[company_name]'];

            if ($temp['accountType'] != 1) {
                $company_validated = $comp['[active_status]'];
                $data['[validated]'] = $company_validated;
                if ($company_validated == 1 || $company_validated == 2) {
                    $data['[company_phone]'] = $comp['[company_phone]'];
                    $data['[company_address]'] = $comp['[company_address]'];
                    $data['[company_city]'] = $comp['[company_city]'];
                    $data['[company_country]'] = $comp['[company_country]'];
                }
            }
            $cache->set("user_array_" . $user_id, $data, 3600);
            return $data;
        }
        else
            return $user;

	}

	function addArrayUserMeta($user_id, $data)
	{
		$mysqli = DBConnection::instance()->db();

		foreach ($data as $key => $value) 
		{
            $mysqli->rawQuery("INSERT INTO user_data (user_id, data, value) VALUES ($user_id, '$key', '$value');");
		}

	}

	function addUserMeta($user_id, $data, $value)
	{
		$mysqli = DBConnection::instance()->db();

        $data = array( "user_id" => $user_id,
                        "data" => $data,
                        "value" => $value);

        $meta = $mysqli->insert("user_data", $data);

        $cache = phpFastCache();
        if ($cache->isExisting("user_array_" . $user_id))
            $cache->delete("user_array_" . $user_id);

        if ($meta)
            return true;
        else
            return false;
	}

	function getUserMeta($user_id, $meta)
	{
		$mysqli = DBConnection::instance()->db();

        $value = $mysqli->where("user_id", $user_id)
            ->where("data", $meta)
            ->getOne("user_data", "value");

        return $value['value'];

	}

    function getAllUserMeta($user_id)
    {
        $mysqli = DBConnection::instance()->db();

        $meta = $mysqli->where("user_id", $user_id)
                ->get("user_data", null, array('data', 'value'));
        $data = array();
        foreach ($meta as $m) {
            $data[$m['data']] = $m['value'];
        }
        return $data;
    }

	function getUserId($username)
	{
		$mysqli = DBConnection::instance()->db();

        $id = $mysqli->where("username", $username)
            ->getOne("user", "id");
        return $id['id'];
	}

	function login_check() 
	{
		$mysqli = DBConnection::instance()->db();
		if (isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) 
		{
		    $user_id = $_SESSION['user_id'];
			$login_string = $_SESSION['login_string'];
			$username = $_SESSION['username'];

			$user_browser = $_SERVER['HTTP_USER_AGENT'];

            $pass = $mysqli->where("id", $user_id)
                ->getOne("user", "password");
            $pass = $pass['password'];

			if ($mysqli->count == 1)
			{
				$login_check = hash('sha512', $pass . $user_browser);
				if ($login_check == $login_string)
					return true;
				else
					return false;
			}
			else
				return false;

		} 
		else 
			return false;
	}


	function login($email, $password) 
	{
		$mysqli = DBConnection::instance()->db();

        $user = $mysqli->where("email", $email)->getOne("user", array("id", "username", "password", "salt"));

        $user_id = $user['id'];
        $username = $user['username'];
        $db_password = $user['password'];
        $salt = $user['salt'];

		$password = hash('sha512', $password.$salt);

		if($mysqli->count == 1)
		{
			if(checkbrute($user_id) == true)
				return false;
			else
			{
				if($db_password == $password)
				{
					$user_browser = $_SERVER['HTTP_USER_AGENT'];
					$user_id = preg_replace("/[^0-9]+/", "", $user_id);
					$_SESSION['user_id'] = $user_id;

					$username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
					$_SESSION['username'] = $username;
					$_SESSION['login_string'] = hash('sha512', $password . $user_browser);
					return true;
				}
				else
				{
					$now = time();
                    $data = array("user_id" => $user_id,
                                "time" => $now);
                    $mysqli->insert("login_attempts", $data);

					return false;
				}
			}
		}
		else
			return false;

	}

	function checkbrute($user_id) 
	{
		$now = time();
		$valid_attempts = $now - (2 * 60 * 60);

		$mysqli = DBConnection::instance()->db();

        $attempts = $mysqli->where("user_id", $user_id)->where("time", $valid_attempts, ">")->get("login_attempts");

        if($mysqli->count > 5)
			return true;
		else
			return false;

	}

	function register($username, $email, $password, $accType)
	{
		$mysqli = DBConnection::instance()->db();
		$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$password = hash('sha512', $password . $random_salt);

		if(chkacc($username, $email))
		{
            $data = array("username" => $username,
                "email" => $email,
                "password" => $password,
                "salt" => $random_salt,
                "accountType" => $accType,
                "join_date" => $mysqli->now());
            $u_id = $mysqli->insert("user", $data);

            if ($u_id)
                return $u_id;
            else
                return -2;
		}
		else
			return -1;
	}

    function changePass($user_id, $password)
    {
        $mysqli = DBConnection::instance()->db();
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $password = hash('sha512', $password . $random_salt);

        $data = array('password' => $password,
                        'salt' => $random_salt);

        $mysqli->where('id', $user_id)->update('user', $data);
    }

	function chkacc($username, $email)
	{
		$mysqli = DBConnection::instance()->db();

        $mysqli->where("email", $email)
                ->orWhere("username", $username)
                ->getOne("user", "id");

        if ($mysqli->count == 1)
            return false;
        else
            return true;
	}

	function checkLang()
	{
		$user_id = $_SESSION['user_id'];
		if ($lang = getUserMeta($user_id, '[lang]'))
			return $lang;
		else
			return -1;
	}

	function getFirstName()
	{
		$user_id = $_SESSION['user_id'];
		if ($name = getUserMeta($user_id, '[firstname]'))
			return $name;
		else
			return -1;
	}

	function getCountryById($id)
	{
		$mysqli = DBConnection::instance()->db();

        $country = $mysqli->where("id", $id)
            ->getOne("country", "name");

        return $country['name'];

	}


	function getAccountType($id)
	{
		$mysqli = DBConnection::instance()->db();

        $accType = $mysqli->where("id", $id)
                        ->getOne("user", array("accountType", "privs", "email", "join_date"));
        return $accType;
	}


    function verify($user, $id)
    {
        $mysqli = DBConnection::instance()->db();

        $code = $mysqli->where("user_id", $user)
                    ->getOne("verify_mail", "activation_code");

        $code = $code['activation_code'];

        if ($code == $id)
        {
            $data = array("privs" => 0);
            $mysqli->where('id', $user)->update("user", $data);
            $cache = phpFastCache();
            if ($cache->isExisting("user_array_" . $user))
                $cache->delete("user_array_" . $user);
            return true;
        }
        else
            return false;
    }

    function addPendingValidation($user_id, $method)
    {
        $mysqli = DBConnection::instance()->db();
        $data = array("user_id" => $user_id, "validation_method" => $method);
        $pending_id = $mysqli->insert("pending_validations", $data);
        return $pending_id;
    }

    function addValidationData($validation_id, $val)
    {
        $mysqli = DBConnection::instance()->db();
        $data = array("pending_id" => $validation_id, "data" => $val);
        $mysqli->insert("validation_data", $data);
        return 1;
    }

    function updateUserInfo($user_id, $data) {
        $mysqli = DBConnection::instance()->db();
        $mysqli->where('id', $user_id)->update('user', $data);
        $cache = phpFastCache();
        if ($cache->isExisting("user_array_" . $user_id))
            $cache->delete("user_array_" . $user_id);
    }

    function updateUserMeta($user_id, $data) {

        $mysqli = DBConnection::instance()->db();
        foreach($data as $k => $v) {
            $mysqli->where('user_id', $user_id)
                    ->where('data', $k)
                    ->update('user_data', array('value' => $v));
            if ($mysqli->count == 0)
                $mysqli->insert('user_data', array('user_id' => $user_id, 'data' => $k, 'value' => $v));
        }

        $cache = phpFastCache();
        if ($cache->isExisting("user_array_" . $user_id))
            $cache->delete("user_array_" . $user_id);
    }

	/*********************************************************************************************
	*	COMPANY FUNCTIONS
	**********************************************************************************************/

	function registerCompany($user_id)
	{
		$mysqli = DBConnection::instance()->db();

        $data = array("user_id" => $user_id);

        $company = $mysqli->insert("company", $data);

        if ($company)
            return $company;
        else
            return -1;
	}

	function addCompanyMeta($id, $data, $value)
	{
		$mysqli = DBConnection::instance()->db();

        $data = array("company_id" => $id,
                    "data" => $data,
                    "value" => $value);

        $mysqli->insert("company_data", $data);
	}

	function addCompanyMetaArray($id, $data)
	{
		$mysqli = DBConnection::instance()->db();

		foreach ($data as $key => $value) 
		{
            $mysqli->rawQuery("INSERT INTO company_data (company_id, data, value) VALUES ('$id', '$key', '$value');");
		}

	}

	function getCompanyMeta($id, $meta)
	{
		$mysqli = DBConnection::instance()->db();

        $val = $mysqli->where("company_id", $id)->where("data", $meta)->getOne("company_data", "value");

        return $val['value'];
	}

    function getAllCompanyMeta($id)
    {
        $mysqli = DBConnection::instance()->db();

        $val = $mysqli->where("company_id", $id)->get("company_data", null, array("data", "value"));
        $data = array();
        foreach($val as $m) {
            $data[$m['data']] = $m['value'];
        }
        return $data;
    }

	function getUserCompany($user_id)
	{
		$mysqli = DBConnection::instance()->db();

        $c_id = $mysqli->where("user_id", $user_id)->getOne("company", "id");

        return $c_id['id'];
	}

    function getCompanyUser($company_id)
    {
        $mysqli = DBConnection::instance()->db();
        $u_id = $mysqli->where("id", $company_id)->getOne("company", "user_id");
        return $u_id['user_id'];
    }

	function checkCompany($company_name)
	{
		$mysqli = DBConnection::instance()->db();

        $res = $mysqli->where("data", "[company_name]")->where("value", $company_name)->getOne("company_data", "company_id");

        if ($mysqli->count == 1)
            return 0;
        else
            return 1;
	}

	function isValidated($id)
	{
		$mysqli = DBConnection::instance()->db();

        $res = $mysqli->where("company_id", $id)->where("data", "[active_status]")->getOne("company_data", "value");
        return $res['value'];
	}

    function updateCompanyMeta($company_id, $data) {
        $mysqli = DBConnection::instance()->db();
        foreach($data as $k => $v) {
                $mysqli->where('company_id = ? AND data = ?', array($company_id, $k))
                    ->update('company_data', array('value' => $v));
                if ($mysqli->count == 0)
                    $mysqli->insert('company_data', array('company_id' => $company_id, 'data' => $k, 'value' => $v));
        }

    }
?>