<?php
    include_once("mysqlconnection.php");
    include_once(__DIR__ . '/../phpfastcache.php');

class PaymentMethods {

    public static function addPaymentMethods($user, $data) {
        $mysqli = DBConnection::instance()->db();

        $mysqli->where("user_id", $user)->delete("payment_methods");

        foreach($data as $method)
            $mysqli->insert("payment_methods", $method);

        $cache = phpFastCache();
        if ($cache->isExisting("user_array_" . $user))
            $cache->delete("user_array_" . $user);
    }

    public static function getPaymentMethods($user) {
        $mysqli = DBConnection::instance()->db();

        $temp = $mysqli->where("user_id", $user)
                         ->get("payment_methods", null, array("method_id", "payment_value"));

        $data = array();

        foreach($temp as $method) {
            $data[$method['method_id']] = $method['payment_value'];
        }
        return $data;
    }
}

?>