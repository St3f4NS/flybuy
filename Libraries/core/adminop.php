<?php
	include_once("mysqlconnection.php");
    include_once(__DIR__ . "/../mail_handler.php");

	function isAdmin($userId)
	{
		$mysqli = DBConnection::instance()->db();

        $priv = $mysqli->where("id", $userId)->getOne("user", "privs");

        return $priv['privs'];
	}

	function countUsers()
	{
		$mysqli = DBConnection::instance()->db();

        $c = $mysqli->getOne("user", "COUNT(id) as cnt");

        return $c['cnt'];
	}

	function countSpecific($type)
	{
		$mysqli = DBConnection::instance()->db();

        $c = $mysqli->where("accountType", $type)->getOne("user", "count(id) as cnt");

        return $c['cnt'];
	}

	function countProducts()
	{
		$mysqli = DBConnection::instance()->db();

        $c = $mysqli->getOne("products", "count(id) as cnt");

        return $c['cnt'];
	}

    function getPendingValidations()
    {
        $mysqli = DBConnection::instance()->db();

        $data = $mysqli->orderBy('pending_id', 'ASC')->get('pending_validations', null, array('pending_id', 'user_id', 'validation_method'));
        return $data;
    }

    function getSelectedValidation($company_id, $validation_id)
    {
        $mysqli = DBConnection::instance()->db();

        $r = array();

        $data = $mysqli->where('pending_id', $validation_id)->get('validation_data', null, 'data');

        foreach($data as $v) {
            $r['proof'][] = $v;
        }


        $submited_info = $mysqli->where('company_id', $company_id)
            ->get('company_data', null, array('data', 'value'));

        foreach ($submited_info as $row) {
            $r['submited'][] = array($row['data'] => $row['value']);
        }
        return $r;
    }

    function setApproved($id, $validation_id)
    {
        $mysql = DBConnection::instance()->db();

        $to_update = array('value' => 1);

        $res = $mysql->where('company_id', $id)->where('data', '[active_status]')->update('company_data', $to_update);

        $res = $mysql->where('pending_id', $validation_id)
                    ->delete('pending_validations');

        $res = $mysql->where('pending_id', $validation_id)
                    ->delete('validation_data');

        $mail = $mysql->join('company c', 'c.user_id = u.id', 'INNER')
            ->where('c.id', $id)
            ->getOne('user u', 'email');

        approvedMail($mail['email']);

        return 1;
    }

    function setDisapproved($id, $validation_id, $text)
    {
        $mysql = DBConnection::instance()->db();

        $to_update = array('value' => 0);

        $res = $mysql->where('company_id', $id)->where('data', '[active_status]')->update('company_data', $to_update);

        $res = $mysql->where('pending_id', $validation_id)
            ->delete('pending_validations');

        $res = $mysql->where('pending_id', $validation_id)
            ->delete('validation_data');

        $mail = $mysql->join('company c', 'c.user_id = u.id', 'INNER')
                    ->where('c.id', $id)
                    ->getOne('user u', 'email');

        disapprovedMail($mail['email'], $text);
        return 1;
    }
?>