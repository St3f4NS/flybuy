<?php
    include_once(__DIR__ . '/accop.php');
    include_once(__DIR__ . '/mysqlconnection.php');
    include_once(__DIR__ . '/payments.php');
    include_once(__DIR__ . '/product.php');
    include_once(__DIR__ . '/../phpfastcache.php');

    class Orders {

        public static function cartCount($user)
        {
            $mysqli = DBConnection::instance()->db();
            $cache = phpFastCache();
            $cart_cache = $cache->get("cart_count_" . $user);
            if ($cart_cache == null) {
                $cnt = $mysqli->where('user_id', $user)->getOne('cart', 'COUNT(product_id) as c');
                $count = $cnt['c'];
                $cache->set("cart_count_" . $user, $count, 24000);
            }
            else
                $count = $cart_cache;

            return $count;
        }

        public static function addToCart ($user, $product)
        {
            $mysqli = DBConnection::instance()->db();
            $cache = phpFastCache();

            $isthere = $mysqli->where('user_id', $user)->where('product_id', $product)->getOne('cart', 'user_id');
            if ($mysqli->count == 0) {
                $data = array("user_id" => $user, "product_id" => $product);
                $mysqli->insert("cart", $data);
                $cache->increment("cart_count_" . $user, 1);
                return 1;
            }
            else
                return -1;
        }

        public static function removeFromCart ($user, $product)
        {
            $mysqli = DBConnection::instance()->db();
            $cache = phpFastCache();
            $mysqli->where('user_id', $user)->where('product_id', $product)->delete("cart");
            $cache->decrement("cart_count_" . $user, 1);
        }

        public static function emptyCart($user)
        {
            $mysqli = DBConnection::instance()->db();
            $cache = phpFastCache();
            $mysqli->where('user_id', $user)->delete("cart");
            $cache->set("cart_count_" . $user, 0, 24000);
        }

        public static function getCartList($user)
        {
            $mysqli = DBConnection::instance()->db();

            $items = $mysqli->where("user_id", $user)->orderBy('item_id', 'DESC')->get('cart', null, array('item_id', 'product_id'));
            $data = array();
            foreach ($items as $item) {
                $data[] =$item['product_id'];
            }
            return $data;
        }

        public static function getOrders($user)
        {
                $mysqli = DBConnection::instance()->db();
                $data_to_get = array(
                    'order_id',
                    'buyer_id',
                    'product_id',
                    'variation_id',
                    'quantity',
                    'shipping',
                    'shipping_country',
                    'price',
                    'status',
                    'date_added',
                    'payment_method'
                );

                $orders = $mysqli->where('seller_id', $user)->orderBy('order_id', 'DESC')->get('orders', null, $data_to_get);

                $data = array();
                foreach ($orders as $order) {
                    $firstname = getUserMeta($order['buyer_id'], '[firstname]');
                    $lastname = getUserMeta($order['buyer_id'], '[lastname]');
                    $order['buyer_firstname'] = $firstname;
                    $order['buyer_lastname'] = $lastname;
                    $q = $mysqli->where('id', $order['product_id'])->getOne('products', 'name');
                    $order['product_name'] = $q['name'];

                    if ($order['status'] > 2) {
                        $transaction = $mysqli->where('order_id', $order['order_id'])->getOne(
                            'transactions',
                            array('transaction_id', 'amount')
                        );
                        $order['transaction_id'] = $transaction['transaction_id'];
                        $order['amount_paid'] = $transaction['amount'];
                    }

                    $data[] = $order;
                }

            return $data;
        }

        public static function getPurchase($user)
        {
                $mysqli = DBConnection::instance()->db();
                $data_to_get = array(
                    'order_id',
                    'seller_id',
                    'product_id',
                    'variation_id',
                    'quantity',
                    'shipping',
                    'shipping_country',
                    'price',
                    'status',
                    'date_added',
                    'payment_method'
                );

                $orders = $mysqli->where('buyer_id', $user)->orderBy('order_id', 'DESC')->get('orders', null, $data_to_get);
                $data = array();
                foreach ($orders as $order) {
                    $firstname = getUserMeta($order['seller_id'], '[firstname]');
                    $lastname = getUserMeta($order['seller_id'], '[lastname]');
                    $order['seller_firstname'] = $firstname;
                    $order['seller_lastname'] = $lastname;
                    $q = $mysqli->where('id', $order['product_id'])->getOne('products', 'name');
                    $order['product_name'] = $q['name'];

                    if ($order['status'] > 2) {
                        $transaction = $mysqli->where('order_id', $order['order_id'])->getOne(
                            'transactions',
                            array('transaction_id', 'amount')
                        );
                        $order['transaction_id'] = $transaction['transaction_id'];
                        $order['amount_paid'] = $transaction['amount'];
                    }

                    $data[] = $order;
                }

            return $data;
        }

        public static function getSingleOrder($order_id)
        {
            $mysqli = DBConnection::instance()->db();
            $data_to_get = array(
                'order_id',
                'buyer_id',
                'seller_id',
                'product_id',
                'variation_id',
                'quantity',
                'shipping',
                'shipping_country',
                'price',
                'status',
                'notes',
                'date_added',
                'payment_method'
            );

            $order = $mysqli->where('order_id', $order_id)->getOne('orders', $data_to_get);
            $temp = getAccountType($order['seller_id']);
            $order['seller_email'] = $temp['email'];
            $firstname_buyer = getUserMeta($order['buyer_id'], '[firstname]');
            $lastname_buyer = getUserMeta($order['buyer_id'], '[lastname]');

            $order['buyer_firstname'] = $firstname_buyer;
            $order['buyer_lastname'] = $lastname_buyer;
            $buyer_meta = getUserDataArray($order['buyer_id']);

            $order['buyer_data'] = $buyer_meta;
            $q = $mysqli->where('id', $order['product_id'])->getOne('products', 'name');
            $order['product_name'] = $q['name'];

            $order['payment_method_list'] = PaymentMethods::getPaymentMethods($order['seller_id']);

            if ($order['status'] > 2) {
                $transaction = $mysqli->where('order_id', $order['order_id'])->getOne(
                    'transactions',
                    array('transaction_id', 'amount')
                    );
                $order['transaction_id'] = $transaction['transaction_id'];
                $order['amount_paid'] = $transaction['amount'];
            }

            $product = new Product($order['product_id']);
            $product_data = $product->getProduct();
            $order['product_currency'] = $product_data['currency'];
            $order['product_variation'] = $product_data['variations'][$order['variation_id']];
            return $order;
        }

        /*
         * @param $data[0] - product_id
         * @param $data[1] - variation_id
         * @param $data[2] - quantity
         * @param $data[3] - price
         * @param $data[4] - owner_id
         * @param $data[5] - shipping_id
         * @param $data[6] - shipping_country
         * @param $data[7] - order_notes
         * @param $data[8] - payment_method
         * @param $user - user thats buying
         */
        public static function addOrder($user, $data)
        {
            $product_id = $data[0];
            $variation_id = $data[1];
            $quantity = $data[2];
            $price = $data[3];
            $owner_id = $data[4];
            $shipping_id = $data[5];
            $shipping_country = $data[6];
            $order_notes = $data[7];
            $payment_method = $data[8];
            $buyer_id = $user;

            $mysqli = DBConnection::instance()->db();

            $data = array('buyer_id' => $buyer_id,
                            'seller_id' => $owner_id,
                            'product_id' => $product_id,
                            'variation_id' => $variation_id,
                            'quantity' => $quantity,
                            'shipping' => $shipping_id,
                            'shipping_country' => $shipping_country,
                            'price' => $price,
                            'status' => 0,
                            'notes' => $order_notes,
                            'date_added' => $mysqli->NOW(),
                            'payment_method' => $payment_method);

            $id = $mysqli->insert('orders', $data);
            if ($id) {
                $result = array();
                $cache = phpFastCache();
                $cache->increment("orders_count_" . $data['seller_id'], 1);

                $firstname = getUserMeta($data['seller_id'], '[firstname]');
                $lastname = getUserMeta($data['seller_id'], '[lastname]');
                $mail = $mysqli->where('id', $owner_id)
                                 ->getOne('user', 'email');

                $result['order_id'] = $id;
                $result['seller'] = $firstname . " " . $lastname;
                $result['seller_mail'] = $mail['email'];
                $result['user_id'] = $owner_id;
                return $id;
            }
            else
                return -1;
        }

        public static function ordersCount($user)
        {
            $cache = phpFastCache();
            $orders_cache = $cache->get("orders_count_" . $user);

            if ($orders_cache == null) {
                $mysqli = DBConnection::instance()->db();

                $count = $mysqli->where('seller_id', $user)->getOne('orders', 'COUNT(order_id) as c');

                $data =  $count['c'];
                $cache->set("orders_count_" . $user, $data, 2000);
            }
            else
                $data = $orders_cache;
            return $data;
        }

        public static function addTransaction($order_id, $trans_id)
        {
            $mysqli = DBConnection::instance()->db();
            $data = array('transaction_id' => $trans_id,
                            'order_id' => $order_id,
                            'amount' => 0);
            $ins = $mysqli->insert('transactions', $data);

            $update_data = array('status' => 6);
            $up = $mysqli->where('order_id', $order_id)->update('orders', $update_data);
            if ($ins)
                return 1;
            else
                return -1;
        }

        public static function updateTransaction($new_status, $transaction_id, $amount = -1)
        {
            $mysqli = DBConnection::instance()->db();

            if ($amount != -1)
                $update = $mysqli->where('transaction_id', $transaction_id)->update('transactions', array('amount' => $amount));

            $order_id = $mysqli->where('transaction_id', $transaction_id)->getOne('transactions', 'order_id');

            $update_data = array('status' => $new_status);

            $up = $mysqli->where('order_id', $order_id['order_id'])->update('orders', $update_data);

            $response = array('order_id' => $order_id, 'new_status' => $new_status);

            if ($amount != -1)
                $response['paid_amount'] = $amount;

            return $response;
        }

        public static function approvement($order_id, $replay, $note = 0)
        { // if $replay = 1, $note is shipping price
            $mysqli = DBConnection::instance()->db();
            $updateData = array();
            $updateData['status'] = $replay;
            if ($replay == 2)
                $updateData['notes'] = $note;
            else
                $updateData['shipping'] = $note;
            $up = $mysqli->where('order_id', $order_id)->update('orders', $updateData);
            if ($up)
                return 1;
            else
                return -1;
        }
    }

?>