<?php
	include_once('mysqlconnection.php');
	include_once(__DIR__ . '/../phpfastcache.php');

	function metaTranslate($data)
	{
		$cache = phpFastCache();
		$translate = $cache->get('meta_' . $data);

		if ($translate == null)
		{
			$mysqli = DBConnection::instance()->db();

            $val = $mysqli->where("data", $data)->getOne("category_data", "value");

            $cache->set('meta_' . $data, $val['value']);
            return $val['value'];
		}
		else
			return $translate;
	}

    function metaCrypt($data)
    {
        $cache = phpFastCache();

        $translate = $cache->get('meta_crypt_'. $data);

        if ($translate == null)
        {
            $mysqli = DBConnection::instance()->db();

            $val = $mysqli->where("value", $data)->getOne("category_data", "data");

            $cache->set('meta_crypt_' . $data, $val['data']);
            return $val['data'];
        }
        else
            return $translate;
    }
?>