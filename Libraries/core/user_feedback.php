<?php

include_once('mysqlconnection.php');

class UserFeedback
{
    protected $user1; //being commented;
    protected $user2; // commented;

    function __construct($user1, $user2 = -1)
    {
        $this->user1 = $user1;

        if ($user2 != -1)
            $this->user2 = $user2;
    }

    function getAllInfo($multiplier = 0) {
        $raiting = $this->getRaiting();
        $data = $this->getFeedbacks($multiplier);
        $count = $data[0];
        $feedbacks = $data[1];

        return array($count, $feedbacks, $raiting);
    }

    function getFeedbacks($multiplier)
    {
        $mysqli = DBConnection::instance()->db();

        $start = $multiplier * 5;
        $feedbacks = $mysqli->withTotalCount()
            ->where('rated_id', $this->user1)
            ->orderBy('fb_id', 'desc')
            ->get('feedback_user', array($start, 5), array('fb_id', 'rater_id', 'message', 'rate', 'date'));

        $c = $mysqli->totalCount;

        $data = array();
        foreach ($feedbacks as $feedback) {
            $firstname = getUserMeta($feedback['rater_id'], '[firstname]');
            $lastname = getUserMeta($feedback['rater_id'], '[lastname]');
            $img_ver = getUserMeta($feedback['rater_id'], '[img_ver]');

            $feedback['rater_firstname'] = $firstname;
            $feedback['rater_lastname'] = $lastname;
            $feedback['img_ver'] = $img_ver;

            $data[] = $feedback;
        }
        return array($c, $data);
    }

    function getFeedbackCount()
    {
        $mysqli = DBConnection::instance()->db();

        $c = $mysqli->where('rated_id', $this->user1)
                    ->getOne('feedback_user', 'COUNT(fb_id) as c');

        return $c['c'];
    }

    function getRaiting()
    {
        $mysqli = DBConnection::instance()->db();

        $rate = $mysqli->where('rated_id', $this->user1)
                        ->getOne('feedback_user', 'AVG(rate) as r');
        $raiting = floor($rate['r'] * 2) / 2;

        return $raiting;
    }

    function addFeedback($message, $rate)
    {
        if (!$this->checkIfAlreadyRated()) {
            $mysqli = DBConnection::instance()->db();

            $data = array(
                'rated_id' => $this->user1,
                'rater_id' => $this->user2,
                'message' => $message,
                'rate' => $rate,
                'date' => $mysqli->now()
            );

            $id = $mysqli->insert('feedback_user', $data);

            $data['id'] = $id;
            $data['date'] = date('Y-m-d h:m:s');
            $firstname = getUserMeta($this->user2, '[firstname]');
            $lastname = getUserMeta($this->user2, '[lastname]');
            $img_ver = getUserMeta($this->user2, '[img_ver]');

            $data['rater_firstname'] = $firstname;
            $data['rater_lastname'] = $lastname;
            $data['img_ver'] = $img_ver;
            return $data;
        }
        else
            return -1;
    }

    function checkIfAlreadyRated()
    {
        $mysqli = DBConnection::instance()->db();
        $res = $mysqli->where('rated_id', $this->user1)->where('rater_id', $this->user2)->getOne('feedback_user', 'fb_id');
        if ($mysqli->count > 0)
            return true;
        else
            return false;
    }
}