<?php
require_once ("mail_loader.php");
include_once (__DIR__ . "/core/mysqlconnection.php");
include_once (__DIR__ . "/../Configs/Config.php");


function verifyAccount($to, $to_name, $id, $type)
{
    $code = code_generator($id);
    $link = ADDRESS . "/active.php?op=0&id=" . $code . "&user=" . $id . "&type=" . $type;

    $text = '<p>Thank you for registering with FlyBuy. To complete your registration, please verify your email by following the link below:</p><a href="'.$link.'" style="display: block; width: 230px; height: 40px; background-color: #489e54; color: white; text-decoration: none; margin: 40px auto 20px auto; line-height: 40px; text-align: center;">CONFIRM EMAIL ADDRESS</a>';
    $subject = "Verify mail";

    sendMail($to, $to_name, $subject, $text);
    switch ($type) {
        case 0:
            $t = "supplier";
            break;
        case 1:
            $t = "buyer";
            break;
        case 2:
            $t = "both";
    }

    $notif = "<p>New user with name: " . $to_name . " registered. User type: " . $t . " you can view his profile by visiting
    <a href='" . ADDRESS . "/user/" . $id . "'>this link</a>. Email: "  . $to  . "</p>";
    sendMail("simon-27@live.co.uk", "Simon", "New User", $notif);

}

function codeSubmited($to, $u_id, $o_id)
{
    $link = ADDRESS . "/user/" . $u_id . "/single_order/" . $o_id;

    $text = '<p>Buyer has now added the payment code. Check and confirm the code provided from the buyer is correct. If correct, then please ship the product to the buyer as soon as possible. </p><a href="' . $link . '" style="display: block; width: 230px; height: 40px; background-color: #489e54; color: white; text-decoration: none; margin: 40px auto 20px auto; line-height: 40px; text-align: center;">Order link</a>';
    $subject = "Buyer submitted a code!";

    sendMail($to, $to ,$subject, $text);
}

function validationNew($email) {
    $notif = "<p>Pending validation, email: " . $email . "</p>";
    sendMail("simon-27@live.co.uk", "Simon", "Pending Validation", $notif);
}

function newOrderMail($to, $to_name, $order_id, $user_id) {
    $subject = "Pending order on FlyBuy!";

    $link = ADDRESS . "/user/" . $user_id . "/single_order/" . $order_id;
    $text = '<p>You have a new pending order on FlyBuy. To review it please visit following link:</p><a href="'.$link.'" style="display: block; width: 230px; height: 40px; background-color: #489e54; color: white; text-decoration: none; margin: 40px auto 20px auto; line-height: 40px; text-align: center;">PENDING ORDER</a>
            <p>Pending order ID: ' . $order_id . '</p>';

    sendMail($to, $to_name, $subject, $text);
}

function approvedOrder($to, $to_name, $user_id, $order_id) {
    $subject = "Order approvement!";

    $link = ADDRESS . "/user/" . $user_id . "/single_order/" . $order_id;
    $text = "<p>Your pending order with ID: " . $order_id . ", has been approved by a supplier. If you are using Transpact as payment method, please visit your
    <a style='color: #489e54; text-decoration: none;' href='http://transpact.com'>Transpact </a> account, for more information about this order, please visit:
     <a style='color: #489e54; text-decoration: none;' href='" . $link . "'>Order link</a>.</p>";

    sendMail($to, $to_name, $subject, $text);
}

function dissaproveOrder($to, $to_name, $order_id, $note) {
    $subject = "Order denied!";

    $text = "<p>Your recent order with ID: " . $order_id . " has been denied by a supplier, and a reason that supplier
    specified is:<br>\"" . $note . "\"</p>";
    sendMail($to, $to_name, $subject, $text);
}

function approvedMail($to)
{
    $body = "<p>Congratulations, you are now a verified member of FlyBuy! Because you are now verified, customers will be more inclined to buy from you. We hope you enjoy your time at FlyBuy and we look forward to selling your products on our site and building a long and prosperous relationship together.</p>";
    $subject = "Company validated";
    sendMail($to, $to, $subject, $body);
}

function disapprovedMail($to, $msg)
{
    $subject = "Company not validated";
    $txt = "<p>" . $msg . "</p>";
    sendMail($to, $to, $subject, $txt);
}

function code_generator($id)
{
    $code = mt_rand( 1000, 9999 );

    $mysqli = DBConnection::instance()->db();

    $data = array("user_id" => $id, "activation_code" => $code);

    $mysqli->insert("verify_mail", $data);

    return $code;
}

function sendMail($to, $to_name, $subject, $text, $rep_to = null, $rep_name = null)
{
    $mail = new PHPMailer(true);

    $mail->isSMTP();                                 // Set mailer to use SMTP
    $mail->Host = MAIL_HOST;                         // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                          // Enable SMTP authentication
    $mail->Username = MAIL_USERNAME;                 // SMTP username
    $mail->Password = MAIL_PASSWORD;                 // SMTP password
    $mail->SMTPSecure = 'tls';                       // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                               // TCP port to connect to
    $mail->setFrom(MAIL_USERNAME, 'FlyBuy');
//$mail->SMTPDebug = 1;
//echo 1;
    $mail->isHTML(true);
    $mail->addAddress($to, $to_name);
    if ($rep_to != null && $rep_name != null)
        $mail->addReplyTo($rep_to, $rep_name);

    $message = file_get_contents(ADDRESS . '/Templates/html_templates/email_temp.html');
    $msg = str_replace('%text%', $text, $message);
    $mail->msgHTML($msg);

    $mail->Subject = $subject;
    $mail->Body = $msg;
//echo $mail->Body;
//$mail->msgHTML($msg);
//msgHTML also sets AltBody, but if you want a custom one, set it afterwards
//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
//echo $message;
    if (!$mail->send()) {
        return -1;
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo 1;
    }

}


?>