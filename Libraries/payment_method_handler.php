<?php
include_once(__DIR__ . '/core/accop.php');
include_once(__DIR__ . '/core/payments.php');
sec_session_start();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if (isset($request->o)) {
    if ($request->o == 0) { //set payment methods
        if ($request->u != $_SESSION['user_id'])
            echo -1;
        else {
            $methods = $request->m;
            $data = array();
            foreach($methods as $key => $val) {
                if ($val)
                    $data[] = array("user_id" => $request->u,
                                    "method_id" => $key,
                                    "payment_value" => $request->v->$key);
            }
            PaymentMethods::addPaymentMethods($request->u, $data);
        }
    }
}

?>