<?php
    include_once('phpfastcache.php');
    include_once(__DIR__ . '/core/mysqlconnection.php');

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->op))
    {
        if ($request->op == 1)
        {
                $mysqli = DBConnection::instance()->db();
                $array = array();

                $data = array("id", "name");
                $c = $mysqli->get("country", null, $data);

                foreach($c as $country)
                {
                    $array[] = array("id" => $country['id'], "name" => $country['name']);
                }

                echo json_encode($array);

        }

        elseif ($request->op == 2)
        {
            $cache = phpFastCache();
            $states_cache = $cache->get("state_c_" . $request->countery);
            if ($states_cache == null)
            {
                $mysqli = DBConnection::instance()->db();

                $fields = array("id", "name");

                $s = $mysqli->where("country_id", $request->countery)->get("state", null, $fields);

                $data = array();

                foreach($s as $state)
                {
                    $data[] = array("id" => $state['id'], "name" => $state['name']);
                }

                $cache->set("state_c_" . $request->countery, $data, 3600);
                echo json_encode($data);
            }
            else
                return $states_cache;
        }
    }

?>