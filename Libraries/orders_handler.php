<?php
    include_once(__DIR__ . "/mail_handler.php");
    include_once(__DIR__ . "/core/accop.php");
    include_once(__DIR__ . "/core/escrow.php");
    include_once(__DIR__ . "/core/orders.php");
    sec_session_start();

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if(isset($request->o)) {
        switch ($request->o) {
            case 0: //get orders
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $orders = Orders::getOrders($request->u_id);
                    echo json_encode($orders);
                }
                break;
            case 1: //get orders count
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $orders_count = Orders::ordersCount($request->u_id);
                    echo json_encode(array($orders_count));
                }
                break;
            case 2: //get purchase
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $purchase = Orders::getPurchase($request->u_id);
                    echo json_encode($purchase);
                }
                break;
            case 3: //get single order
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $order = Orders::getSingleOrder($request->order);
                    echo json_encode($order);
                }
                break;
            case 4: //new order
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $order = Orders::addOrder($request->u_id, $request->order_data);
                    if ($order == -1)
                        echo json_encode($order);
                    else {
                        echo json_encode($order);
                        newOrderMail($order['seller_mail'], $order['seller'], $order['order_id'], $order['user_id']);
                    }
                }
                break;
            case 5: //aprove/dis order
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    if ($request->replay == 1) {
                        $order = Orders::approvement($request->order_id, $request->replay, $request->shipping_price);
                        if ($order == 1) {
                            if ($request->payment_method == 0) {
                                $transpact = new Transpact();
                                $trans_id = $transpact->newTransaction(
                                    $request->buyer_mail,
                                    $request->seller_mail,
                                    $request->price,
                                    $request->currency
                                );
                                Orders::addTransaction($request->order_id, $trans_id->CreateTranspactResult);
                            }
                            approvedOrder(
                                $request->buyer_mail,
                                $request->user_name,
                                $request->buyer_id,
                                $request->order_id
                            );
                            echo json_encode(1);
                        }
                        else
                            echo json_encode(-1);
                    }
                    else {
                        $order = Orders::approvement($request->order_id, $request->replay, $request->note);
                        if ($order == 1) {
                            dissaproveOrder(
                                $request->buyer_mail,
                                $request->user_name,
                                $request->order_id,
                                $request->note
                            );
                            echo json_encode(1);
                        }
                        else
                            echo json_encode(-1);
                    }
                }
                break;
            case 6: //get cart count
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $count = Orders::cartCount($request->u_id);
                    echo json_encode(array($count));
                }
                break;
            case 7: //get cart list
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $data = Orders::getCartList($request->u_id);
                    echo json_encode($data);
                }
                break;
            case 8: //remove single item from cart
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else
                    Orders::removeFromCart($request->u_id, $request->product);
                break;
            case 9: //empty whole cart
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else
                    Orders::emptyCart($request->u_id);
                break;
            case 10: //add to cart
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    $answer = Orders::addToCart($request->u_id, $request->product);
                    echo json_encode($answer);
                }
                break;
            case 11: // add transpact
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    if ($request->transemail != null) {
                        if ($request->update == 0) {
                            addUserMeta($request->u_id, '[transpact]', $request->transemail);
                        } else {
                            $data = array("[transpact]" => $request->transemail);
                            updateUserMeta($request->u_id, $data);
                        }
                    }
                    echo json_encode(1);
                }
                break;
            case 12: //add transaction code etc.
                if ($request->u_id != $_SESSION['user_id'])
                    echo json_encode(-1);
                else {
                    Orders::addTransaction($request->order, $request->code);
                    codeSubmited($request->s_mail, $request->s_id, $request->order);
                    echo json_encode(1);
                }

                break;
        }
    }


?>