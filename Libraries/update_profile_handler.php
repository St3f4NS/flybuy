<?php
include_once("image_handler.php");
include_once("mail_handler.php");
include_once(__DIR__ . '/core/accop.php');
sec_session_start();

if ($_SESSION['user_id'] != $_POST['user_id'])
    header("Location: ../");

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
    header("Location: ../");

    $user_id = $_POST['user_id'];
    $email = $_POST['email'];
    $password = $_POST['p'];
    $country = $_POST['country'];
    $postCode = $_POST['postCode'];
    $uCity = $_POST['uCity'];
    $address = $_POST['address'];
    $fName = $_POST['fName'];
    $lName = $_POST['lName'];
    $companyName = $_POST['companyName'];
    $phone = $_POST['phone'];


    if (preg_match_all('~^' . "number:(?'id'\d+)" . '$~i', $country, $match))
        $country = $match[1][0];

    $user_info = array();
    $user_data = array();
    $user = getUserDataArray($user_id);

    if ($password != -1) {
        changePass($user_id, $password);
    }
    if ($email != $user['[email]']) {
        $user_info['email'] = $email;
        $user_info['privs'] = 2;
        verifyAccount($email, $fName . " " . $lName, $user_id, $user['[accType]']);
    }
    if (isset($user['[country]']))
        if ($country != $user['[country]']) {
            $user_data['[country]'] = $country;
        }
    else
        $user_data['[country]'] = $country;

    if (in_array('[post_code]', $user))
        if ($postCode != $user['[post_code]']) {
            $user_data['[post_code]'] = $postCode;
        }
    else
        $user_data['[post_code]'] = $postCode;

    if (in_array('[ucity]', $user))
        if ($uCity != $user['[ucity]']) {
            $user_data['[ucity]'] = $uCity;
        }
    else
        $user_data['[ucity]'] = $uCity;

    if (in_array('[address]', $user))
        if ($address != $user['[address]']) {
            $user_data['[address]'] = $address;
        }
    else
        $user_data['[address]'] = $address;


    if ($fName != $user['[firstname]']) {
        $user_data['[firstname]'] = $fName;
    }
    if ($lName != $user['[lastname]']) {
        $user_data['[lastname]'] = $lName;
    }

    if (in_array('[telephone]', $user))
        if ($phone != $user['[telephone]']) {
            $user_data['[telephone]'] = $phone;
        }
    else
        $user_data['[telephone]'] = $phone;

    if (!empty($user_info))
        updateUserInfo($user_id, $user_info);
    if (!empty($user_data))
        updateUserMeta($user_id, $user_data);

    if(isset($_POST['enable_validation'])) {
        $val_method = $_POST['v_method'];
        if ($val_method == 0)
        {
            addPendingValidation($user['[company_id]'], $val_method);

            $vatC = $_POST['vatC'];
            $cPhone = $_POST['cPhone'];
            $cAddress = $_POST['cAddress'];
            $cCity = $_POST['cCity'];
            $vat = $_POST['vat'];

            $company_data = array('[company_name]' => $companyName,
                '[company_phone]' => $cPhone,
                '[company_address]' => $cAddress,
                '[company_city]' => $cCity,
                '[company_country]' => $vatC,
                '[vat_code]' => $vat,
                '[active_status]' => 2);
        }
        else if ($val_method == 1)
        {
            $pending_id = addPendingValidation($user['[company_id]'], $val_method);

            $temp = explode("+", $_POST['validation_imgs']);

            foreach($temp as $img) {
                if ($img != "") {
                    $new_img = str_replace("temp","val_img", $img);
                    renameImage($img, $new_img);
                    addValidationData($pending_id, $new_img);
                }
            }

            $vatC = $_POST['vatC'];
            $cPhone = $_POST['cPhone'];
            $cAddress = $_POST['cAddress'];
            $cCity = $_POST['cCity'];

            $company_data = array('[company_name]' => $companyName,
                '[company_phone]' => $cPhone,
                '[company_address]' => $cAddress,
                '[company_city]' => $cCity,
                '[company_country]' => $vatC,
                '[active_status]' => 2);
        }
        else {
            $company_data = array('[company_name]' => $companyName);
        }
        updateCompanyMeta($user['[company_id]'], $company_data);
    }
    else {

        if (in_array('[company_name]', $user))
            if ($companyName != $user['[company_name]']) {
                $company_data['[company_name]'] = $companyName;
                updateCompanyMeta($user['[company_id]'], $company_data);
            }
        else {
            $company_data['[company_name]'] = $companyName;
            updateCompanyMeta($user['[company_id]'], $company_data);
        }
    }

    if ($password != -1)
        header("Location: ../");
    else
        header("Location: ../#!/edit/profile");

?>