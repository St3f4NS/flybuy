<?php

    include_once (__DIR__ . '/core/mysqlconnection.php');

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->e))
        if(filter_var($request->e, FILTER_VALIDATE_EMAIL))
            echo addEmail($request->e);
        else
            echo "Please use valid email form.";


    function addEmail($mail)
    {
        $mysqli = DBConnection::instance()->db();

        $data = array('email' => $mail);

        $check = $mysqli->where('email', $mail)->getOne('subscribers', 'id');

        if ($mysqli->count == 0) {
            $new = $mysqli->insert('subscribers', $data);

            if ($new)
                $msg = "Email has been added to the list of subscribers!";
            else
                $msg = "Error while adding email to list";

            return $msg;
            }

    }
?>