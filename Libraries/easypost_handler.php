<?php
    include_once (__DIR__ . '/../Configs/Config.php');
    include_once (__DIR__ . '/easypost/easypost.php');

    \EasyPost\EasyPost::setApiKey(EASYPOST_API);


$shipment = \EasyPost\Shipment::create(array(
    "to_address" => array(
        'name' => 'Dr. Steve Brule',
        'street1' => '179 N Harbor Dr',
        'city' => 'Redondo Beach',
        'state' => 'CA',
        'zip' => '90277',
        'country' => 'US',
        'phone' => '3331114444',
        'email' => 'dr_steve_brule@gmail.com'
    ),
    "from_address" => array(
        'name' => 'EasyPost',
        'street1' => '164 Townsend',
        'zip' => 'L1',
        'country' => 'UK',
        'phone' => '3331114444',
        'email' => 'support@easypost.com'
    ),
    "parcel" => array(
        "length" => 20.2,
        "width" => 10.9,
        "height" => 5,
        "weight" => 65.9
    )
));
ini_set('xdebug.var_display_max_depth', 5);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);
var_dump($shipment);
/*foreach($shipment['rates'] as $rate) {
    echo $rate['carrier'] . "<br>";
    echo $rate['service'] . "<br>";
    echo $rate['rate']. "<br>";
}*/