<?php
    include_once(__DIR__ . '/core/sitemap_generator.php');
    include_once(__DIR__ . '/../Configs/Config.php');

    function addToSitemap($url) {
        $sitemap = new SitemapGenerator(ADDRESS, "../");
// will create also compressed (gzipped) sitemap
        $sitemap->createGZipFile = true;
// determine how many urls should be put into one file
        $sitemap->maxURLsPerSitemap = 10000;
// sitemap file name
        $sitemap->sitemapFileName = "sitemap.xml";
// sitemap index file name
        $sitemap->sitemapIndexFileName = "sitemap-index.xml";
// robots file name
        $sitemap->robotsFileName = "robots.txt";

// add many URLs at one time
        $sitemap->addUrl(ADDRESS . '/#!/' . $url,                date('c'),  'daily',    '1');
        //$sitemap->createSitemap();
        // write sitemap as file
       // $sitemap->writeSitemap();

        try {
            // create sitemap
            $sitemap->createSitemap();
            // write sitemap as file
            $sitemap->writeSitemap();
            // update robots.txt file
            //$sitemap->updateRobots();
            // submit sitemaps to search engines
            $result = $sitemap->submitSitemap();
            // shows each search engine submitting status

        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
