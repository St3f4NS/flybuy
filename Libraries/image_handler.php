<?php
	include_once (__DIR__ . '/../Configs/Config.php');
	include_once (__DIR__ . '/cloudinary/Api.php');
	include_once (__DIR__ . '/cloudinary/Cloudinary.php');
	include_once (__DIR__ . '/cloudinary/Uploader.php');
    include_once (__DIR__ . '/core/accop.php');

	\Cloudinary::config(array( 
	  "cloud_name" => CLOUDINARY_CLOUD, 
	  "api_key" => CLOUDINARY_API, 
	  "api_secret" => CLODUINARY_SECRET
	));

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    if (isset($request->u))
    {
        if (isset($request->i))
            setProfImg($request->u, $request->i, $request->v);
    }

	function renameImage($oldName, $newName)
	{
		\Cloudinary\Uploader::rename($oldName, $newName, array("overwrite" => TRUE));
	}

    function setProfImg($uid, $img_id, $img_v)
    {
        $newName = "u/" . $uid . "/prof";

        \Cloudinary\Uploader::rename($img_id, $newName, array("overwrite" => TRUE));
        $data = array('[img_ver]' => $img_v);
        updateUserMeta($uid, $data);
    }

    function uploadProfileDefault($id)
    {
        $img = \Cloudinary\Uploader::upload('https://res.cloudinary.com/'. CLOUDINARY_CLOUD . '/image/upload/default-user.png', array("public_id" => "u/" . $id . "/prof"));
        $vers = $img['version'];
        addUserMeta($id, '[img_ver]', $vers);
    }

?>