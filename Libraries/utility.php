<?php
	function getCpuUsage()
	{
		$load = sys_getloadavg();
		return $load[0];
	}
	function getMemUsage()
	{
		$free = shell_exec('free');
		$free = (string)trim($free);
		$free_arr = explode("\n", $free);
		$mem = explode(" ", $free_arr[1]);
		$mem = array_filter($mem);
		$mem = array_merge($mem);
		$memory_usage = $mem[2]/1024;
		$full_mem = $mem[1]/1024;
		return array($full_mem, $memory_usage);
	}
?>