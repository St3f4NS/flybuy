<?php
   include_once(__DIR__ . '/core/accop.php');
   sec_session_start();

   if(isset($_POST['email'], $_POST['p'])) 
   { 
      if (!empty($_POST['email']) && !empty($_POST['p']))
      {
         $email = $_POST['email'];
         $password = $_POST['p'];
         if(filter_var($email, FILTER_VALIDATE_EMAIL))
         {
            if(login($email, $password))
               header("Location: ../");
            else 
               header("Location: ../index.php?error=invalid");
         }
         else
            header("Location: ../index.php?error=mail");
      }
      else
         header("Location: ../index.php?error=fields");
   } 
   else 
      header("Location: ../404.php");

?>