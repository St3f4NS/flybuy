<?php
	include_once(__DIR__ . '/core/accop.php');
	include_once(__DIR__ . '/core/adminop.php');
   	sec_session_start();

	if (isset($_POST['email'], $_POST['p'])) 
	{
		 if (!empty($_POST['email']) && !empty($_POST['p']))
		 {
		 	$email = $_POST['email'];
		 	$pass = $_POST['p'];
		 	if(!login($email, $pass))
		 		header("Location: ../FB-Admin/index.php?error=invalid");
		 	else
		 	{
		 		if (isAdmin($_SESSION['user_id']) == 1)
		 			header("Location: ../FB-Admin/index.php"); 
		 		else
		 			header("Location: ../Libraries/logout_handle.php");
		 	}
		 }
	}
?>