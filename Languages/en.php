<?php
	define("INVALID_LOGIN", "Wrong email and/or password.");
	define("MAIL_FORM", "Please use valid email form.");
	define("FIELDS_INCOMPLETE", "Please enter both, your email and password.");
	define("FAILED_TO_REGISTER", "Failed to register.");
	define("ACCOUNT_EXIST", "Account with that username/email already exists.");
	define("COMPANY_EXIST", "Company with that name already exists.");
?>