<?php
    include_once(__DIR__ . '/Configs/Config.php');
    include_once(__DIR__ . '/Libraries/image_handler.php');
    include_once(__DIR__ . '/Libraries/core/accop.php');
    include_once(__DIR__ . '/Libraries/Twig/Autoloader.php');
    include_once(__DIR__ . '/Languages/index.php');
    sec_session_start();

	if(login_check())
	{
		ob_start();
        header("Location: /");
		$content = ob_get_clean();
	}
	else
	{
        Twig_Autoloader::register();

        $templateLoc = __DIR__ . '/Templates/';

        $loader = new Twig_Loader_Filesystem($templateLoc);

        $twig = new Twig_Environment($loader, array(/*'cache' => __DIR__  . '/cache/'*/));

        $params = array();
        $header = $twig->loadTemplate('html_templates\header_logout.html');
        $footer = $twig->loadTemplate('html_templates\footer.html');
        $params['header'] = $header;
        $params['footer'] = $footer;
        $params['path'] = ADDRESS . "/Templates/";
        $params['cloudinary'] = CLOUDINARY_CLOUD;
        $params['status'] = 2;
        $params['cloudinary_config'] = cloudinary_js_config();
        $params['upload_tag'] = cl_image_upload_tag('register', array("folder" => "val_img", "allowed_formats" => ['png']));


		ob_start();

		if (isset($_GET["error"]))
		{
			$msg = $_GET["error"];
			switch ($msg) 
			{
				case 'mail':
                    $t_msg = MAIL_FORM;
					break;
				case 'exist':
                    $t_msg = ACCOUNT_EXIST;
					break;
				case 'failed':
                    $t_msg = FAILED_TO_REGISTER;
					break;
				case 'company_exist':
                    $t_msg = COMPANY_EXIST;
					break;
				default:
					break;
			}
            $params['error'] = $t_msg;
		}
        $template = $twig->loadTemplate('html_templates\register.html')->display($params);
		$content = ob_get_clean();
	}
	print($content);
?>