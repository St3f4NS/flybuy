<?php
	include_once(dirname(__FILE__). '/../Configs/Config.php');
	include_once(dirname(__FILE__) . '/../Libraries/categories.php');
    include_once(dirname(__FILE__) . '/../Libraries/utility.php');
    include_once(dirname(__FILE__) . '/../Libraries/core/accop.php');
    include_once(dirname(__FILE__) . '/../Libraries/core/adminop.php');
	include_once(dirname(__FILE__) . '/../Libraries/core/messages.php');
	include_once(dirname(__FILE__) . '/../Libraries/Twig/Autoloader.php');
	include_once(dirname(__FILE__). '/../Languages/index.php');
	sec_session_start();

	Twig_Autoloader::register();
	$templateLoc = $_SERVER["DOCUMENT_ROOT"] . '/Templates/admin';

	$loader = new Twig_Loader_Filesystem($templateLoc);

	$twig = new Twig_Environment($loader, array(/*'cache' => dirname(__FILE__) . '/../cache/'*/));
	$header = $twig->loadTemplate('header.html');
	$footer = $twig->loadTemplate('footer.html'); 

	$error = "";

	if (isset($_GET['error']))
	{
		$msg = $_GET['error'];

		switch ($msg) 
		{
			case 'invalid':
				$error = INVALID_LOGIN;
				break;
			
			default:
				# code...
				break;
		}
	}
	
	if (login_check() && (isAdmin($_SESSION['user_id']) == 1))
	{
		$name = getFirstName();
		$messanger = new Messanger($_SESSION['user_id']);
		$messages = $messanger->getConversationsList();
		$newMessages = $messanger->getNewMessages();

		if (isset($_GET['id']))
		{

			//specific
		}
		else
		{
			ob_start();
			$params = array ("header" => $header,
							"footer" => $footer,
							"user_id" => $_SESSION['user_id'],
							"messages" => $messages,
							"newMessages" => $newMessages,
							"path" => ADDRESS,
							"name" => $name);

			$template = $twig->loadTemplate('inbox.html')->display($params);
			$content = ob_get_clean();
		}
	}
	else
	{
		ob_start();
		$params = array( "path" => ADDRESS, "error" => $error);
		$template = $twig->loadTemplate('login.html')->display($params);
		$content = ob_get_clean();
	}

	print($content);
?>