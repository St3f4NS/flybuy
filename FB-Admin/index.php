<?php
	include_once(__DIR__ . '/../Configs/Config.php');
    include_once(__DIR__ . '/../Libraries/categories.php');
    include_once(__DIR__ . '/../Libraries/utility.php');
	include_once(__DIR__ . '/../Libraries/core/accop.php');
	include_once(__DIR__ . '/../Libraries/core/adminop.php');
	include_once(__DIR__ . '/../Libraries/core/messages.php');
	include_once(__DIR__ . '/../Libraries/Twig/Autoloader.php');
	include_once(__DIR__ . '/../Languages/index.php');

    sec_session_start();

	Twig_Autoloader::register();
	$templateLoc = __DIR__ . '/../Templates/admin';

	$loader = new Twig_Loader_Filesystem($templateLoc);

	$twig = new Twig_Environment($loader, array('cache' => __DIR__  . '/../storage/template_cache'));
	$header = $twig->loadTemplate('header.html');
	$footer = $twig->loadTemplate('footer.html'); 
	
	$error = "";

	if (isset($_GET['error']))
	{
		$msg = $_GET['error'];

		switch ($msg) 
		{
			case 'invalid':
				$error = INVALID_LOGIN;
				break;
			
			default:
				# code...
				break;
		}
	}
	if (login_check() && (isAdmin($_SESSION['user_id']) == 1))
	{
		$name = getFirstName();
		$menu = getFullList();
		$messanger = new Messanger($_SESSION['user_id']);
		$messages = $messanger->getConversationsList();
		$newMessages = $messanger->getNewMessages();

		$user_id = $_SESSION['user_id'];

		if (isset($_GET['page']))
		{
			$page = $_GET['page'];

			switch ($page) 
			{
                case 'validations':
                    ob_start();
                    $params = array ("header" => $header,
                        "footer" => $footer,
                        "messages" => $messages,
                        "newMessages" => $newMessages,
                        "path" => ADDRESS,
                        "name" => $name,
                        "cloudinary" => CLOUDINARY_CLOUD,
                        "menu" => $menu);

                    $template = $twig->loadTemplate('pending_validations.html')->display($params);
                    $content = ob_get_clean();
                    break;
				case 'categories':
					ob_start();
					$params = array ("header" => $header,
							"footer" => $footer,
							"messages" => $messages,
							"newMessages" => $newMessages,
							"path" => ADDRESS,
							"name" => $name,
							"menu" => $menu);

					$template = $twig->loadTemplate('categories.html')->display($params);
					$content = ob_get_clean();
					break;
				case 'statistics':
					ob_start();
					$users_count = countUsers();
					$supplier_count = countSpecific(0);
					$buyer_count = countSpecific(1);
					$both_count = countSpecific(2);
					$product_count = countProducts();

					$params = array("header" => $header,
									"footer" => $footer,
									"messages" => $messages,
									"newMessages" => $newMessages,
									"name" => $name,
									"path" => ADDRESS,
									"count_users" => $users_count,
									"supplier_count" => $supplier_count,
									"buyer_count" => $buyer_count,
									"both_count" => $both_count,
									"products_count" => $product_count);
					$template = $twig->loadTemplate('statistics.html')->display($params);
					$content = ob_get_clean();
					break;
			}
		}
		else
		{
			ob_start();
			//$cpu = getCpuUsage()*10; //LINUX COMMANDS
			//$mem =  getMemUsage();
			//$alloc_mem = round($mem[0], 0);
			//$used_mem = round($mem[1]/$alloc_mem*100, 0);

			$params = array ("header" => $header,
							"footer" => $footer,
							"messages" => $messages,
							"newMessages" => $newMessages,
							"path" => ADDRESS,
							"name" => $name,
							"cpu" => 0,//$cpu,
							"ram" => 0,//$used_mem,
							"alloc_ram" => 0,//$alloc_mem,
							"user_id" => $user_id);

			$template = $twig->loadTemplate('index.html')->display($params);

			$content = ob_get_clean();
		}
	}
	else
	{
		ob_start();
		$params = array( "path" => ADDRESS, "error" => $error);
		$template = $twig->loadTemplate('login.html')->display($params);
		$content = ob_get_clean();
	}

	print($content);

	
?>
