CREATE TABLE cart
(
    user_id INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    item_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE categories
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    parent_id INT DEFAULT 0,
    name VARCHAR(30)
);
CREATE TABLE category_data
(
    category_id INT NOT NULL,
    data VARCHAR(32) NOT NULL,
    value VARCHAR(40) NOT NULL
);
CREATE TABLE company
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id BIGINT NOT NULL
);
CREATE TABLE company_data
(
    company_id INT NOT NULL,
    data VARCHAR(30) NOT NULL,
    value VARCHAR(60),
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE conversations
(
    id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user1 BIGINT NOT NULL,
    user2 BIGINT NOT NULL
);
CREATE TABLE country
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    flag VARCHAR(30)
);
CREATE TABLE feedback_product
(
    fb_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id INT UNSIGNED NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    message VARCHAR(255) NOT NULL,
    rate TINYINT UNSIGNED NOT NULL,
    date DATETIME NOT NULL
);
CREATE TABLE feedback_user
(
    fb_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    rated_id INT UNSIGNED NOT NULL,
    rater_id INT UNSIGNED NOT NULL,
    message VARCHAR(100) NOT NULL,
    rate TINYINT UNSIGNED NOT NULL,
    date DATETIME NOT NULL
);
CREATE TABLE login_attempts
(
    id BIGINT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id BIGINT NOT NULL,
    time VARCHAR(30) NOT NULL
);
CREATE TABLE messages
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    conversation_id BIGINT NOT NULL,
    sender BIGINT NOT NULL,
    text VARCHAR(255) NOT NULL,
    seen SMALLINT UNSIGNED DEFAULT 1 NOT NULL,
    date DATE NOT NULL
);
CREATE TABLE orders
(
    order_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    buyer_id INT UNSIGNED NOT NULL,
    seller_id INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    variation_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    shipping INT UNSIGNED DEFAULT 0,
    shipping_country VARCHAR(10) NOT NULL,
    price INT UNSIGNED NOT NULL,
    status TINYINT UNSIGNED NOT NULL,
    date_added DATETIME NOT NULL,
    notes VARCHAR(120) NOT NULL
);
CREATE TABLE pending_validations
(
    pending_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT UNSIGNED NOT NULL,
    validation_method TINYINT NOT NULL
);
CREATE TABLE product_data
(
    product_id INT NOT NULL,
    p_data VARCHAR(30),
    val VARCHAR(30)
);
CREATE TABLE product_discounts
(
    product_id INT NOT NULL,
    val BIGINT,
    disc_type VARCHAR(4),
    quantity BIGINT NOT NULL,
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE product_variations
(
    variation_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    product_id INT NOT NULL
);
CREATE TABLE products
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category_id INT NOT NULL,
    company_id INT NOT NULL,
    name VARCHAR(30) NOT NULL,
    sold INT DEFAULT 0 NOT NULL
);
CREATE TABLE reset_pass
(
    user_id INT UNSIGNED NOT NULL,
    code INT NOT NULL
);
CREATE TABLE sidebar
(
    product_id INT UNSIGNED NOT NULL,
    text VARCHAR(30) NOT NULL
);
CREATE TABLE slideshow
(
    id SMALLINT UNSIGNED PRIMARY KEY NOT NULL,
    img VARCHAR(15) NOT NULL,
    text_top VARCHAR(15) NOT NULL,
    text_bot VARCHAR(20) NOT NULL,
    link VARCHAR(60) NOT NULL
);
CREATE TABLE subscribers
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    email VARCHAR(30) NOT NULL
);
CREATE TABLE subscription_payments
(
    user_id INT UNSIGNED NOT NULL,
    data DATE NOT NULL
);
CREATE TABLE transactions
(
    transaction_id INT UNSIGNED NOT NULL,
    order_id INT UNSIGNED NOT NULL,
    amount INT UNSIGNED NOT NULL
);
CREATE TABLE user
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(128) NOT NULL,
    salt VARCHAR(128) NOT NULL,
    privs INT DEFAULT 2,
    accountType INT NOT NULL,
    join_date DATE NOT NULL
);
CREATE TABLE user_data
(
    user_id INT NOT NULL,
    data VARCHAR(30) NOT NULL,
    value VARCHAR(40),
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE validation_data
(
    data VARCHAR(30) NOT NULL,
    pending_id INT UNSIGNED NOT NULL
);
CREATE TABLE variation_data
(
    variation_id INT NOT NULL,
    p_data VARCHAR(30),
    val VARCHAR(40),
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT
);
CREATE TABLE verify_mail
(
    id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT UNSIGNED NOT NULL,
    activation_code INT UNSIGNED NOT NULL
);
CREATE INDEX cart_user ON cart (user_id);
CREATE INDEX category_id ON category_data (category_id);
CREATE INDEX owner ON company (user_id);
CREATE INDEX `company id` ON company_data (company_id, data);
CREATE INDEX data ON company_data (data);
CREATE INDEX user1 ON conversations (user1, user2);
CREATE INDEX product_id ON feedback_product (product_id);
CREATE INDEX rated_id ON feedback_user (rated_id);
CREATE INDEX user_id ON login_attempts (user_id);
CREATE INDEX conversation_id ON messages (conversation_id);
CREATE INDEX receiver ON messages (sender);
CREATE INDEX buyer_id ON orders (buyer_id);
CREATE INDEX seller_id ON orders (seller_id);
CREATE INDEX product_id ON product_data (product_id, p_data);
CREATE INDEX product_id ON product_discounts (product_id);
CREATE INDEX product_id ON product_variations (product_id);
CREATE INDEX category_id ON products (category_id);
CREATE INDEX company_id ON products (company_id);
CREATE INDEX name ON products (name);
CREATE INDEX sold ON products (sold);
CREATE INDEX user_id ON reset_pass (user_id);
CREATE INDEX product_id ON sidebar (product_id);
CREATE INDEX user_id ON subscription_payments (user_id);
CREATE INDEX transaction_id ON transactions (transaction_id);
CREATE UNIQUE INDEX email ON user (email);
CREATE INDEX user ON user (username);
CREATE INDEX id ON user_data (user_id, data);
CREATE INDEX pending_id ON validation_data (pending_id);
CREATE INDEX variation_id ON variation_data (variation_id, p_data);
CREATE INDEX user_id ON verify_mail (user_id);
