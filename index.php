<?php
    include_once(__DIR__ . '/Configs/Config.php');
    include_once(__DIR__ . '/Libraries/image_handler.php');
    include_once(__DIR__ . '/Libraries/core/accop.php');
    include_once(__DIR__ . '/Libraries/Twig/Autoloader.php');
    include_once(__DIR__ . '/Languages/index.php');

    sec_session_start();

    Twig_Autoloader::register();

    $templateLoc = __DIR__ . '/Templates/';

    $loader = new Twig_Loader_Filesystem($templateLoc);

    $twig = new Twig_Environment($loader, array(/*'cache' => __DIR__  . '/storage/template_cache'*/));

    $params = array();

    if (login_check()) {
        $header = $twig->loadTemplate('html_templates\header_login.html');

        $user = getUserDataArray($_SESSION['user_id']);
        $params['name'] = $user['[firstname]'] . " " . $user['[lastname]'];

        $params['id'] = $user['[id]'];
        $params['img_ver'] = $user['[img_ver]'];
        $params['status'] = 1;
        $params['accType'] = $user['[accType]'];
        $params['active_email'] = $user['[privs]'];
        if (array_key_exists('[transpact]', $user))
            $params['transpact'] = 1;
        else
            $params['transpact'] = 0;
    }
    else {
        $header = $twig->loadTemplate('html_templates\header_logout.html');
        $params['status'] = 0;
    }

    if (isset($_GET['error'])) {
        $error = $_GET['error'];

        switch ($error) {
            case 'invalid':
                $e_msg = INVALID_LOGIN;
                break;
            case 'mail':
                $e_msg = MAIL_FORM;
                break;
            case 'fields':
                $e_msg = FIELDS_INCOMPLETE;
                break;
        }
        $params['error_msg'] = $e_msg;
    }

    $footer = $twig->loadTemplate('html_templates\footer.html');
    $params['header'] = $header;
    $params['footer'] = $footer;
    $params['address'] = ADDRESS;
    $params['path'] = ADDRESS . "/Templates/";
    $params['cloudinary'] = CLOUDINARY_CLOUD;
    $params['cloudinary_config'] = cloudinary_js_config();
    $params['upload_tag'] = cl_image_upload_tag('img', array("folder" => "temp", "format" => "png"));


    ob_start();
    $template = $twig->loadTemplate('html_templates\content.html')->display($params);
    $content = ob_get_clean();

    print($content);

?>
