<?php
	include_once(__DIR__ . '/../Libraries/core/accop.php');
    include_once(__DIR__ . '/../Libraries/core/altorouter.php');
    include_once(__DIR__ . '/../Libraries/core/orders.php');
	include_once(__DIR__ . '/../Libraries/core/product.php');
    include_once(__DIR__ . '/../Libraries/core/product_feedback.php');
	include_once(__DIR__ . '/../Libraries/core/products_list.php');
    include_once(__DIR__ . '/../Libraries/core/user_feedback.php');

    $router = new AltoRouter();
    $router->setBasePath('/API');

    //product related routes
    $router->map('GET', '/product/[i:id]', 'product_get');
    $router->map('GET', '/product/[i:id]/reviews/[i:multiplier]', 'product_reviews_get');
    $router->map('POST', '/product/[i:id]/reviews', 'product_reviews_post');

    //user related routes
    $router->map('GET', '/user/[i:id]', 'user_get');
    $router->map('GET', '/user/[i:id]/reviews/[i:multiplier]', 'user_fb_get');
    $router->map('POST', '/user/[i:id]/reviews', 'user_fb_post');

    //list related routes
    $router->map('GET', '/user/[i:id]/products/[i:multiplier]/[i:per_page]', 'user_list_get');
    $router->map('GET', '/list/[i:id]/[i:multiplier]/[i:per_page]', 'list_get');
    $router->map('GET', '/search/[a:query]/[i:page]', 'search');
    $router->map('GET', '/latest/[i:count]', 'latest');
    $router->map('GET', '/most_selling', 'mostSelling');
    $router->map('GET', '/sidebar', 'sidebar');

    //transpact status update
    $router->map('POST', '/transpact/status', 'transpact_update');

    $match = $router->match();


    switch($match["target"]) {
        case "sidebar":
            $list = ProductList::getSidebar();
            echo json_encode($list);
            break;
        case "product_get":
            $product = new Product($match["params"]["id"]);
            echo json_encode($product->getProduct());
            break;
        case "product_reviews_get":
            $product = new ProductFeedback($match["params"]["id"]);
            echo json_encode($product->getFeedbacks($match["params"]["multiplier"]));
            break;
        case "product_reviews_post":
            $postdata = file_get_contents("php://input");
            $request = json_decode($postdata);
            $c = new ProductFeedback($match["params"]["id"], $request->rater);
            $rate = $c->addFeedback($request->rate_msg, $request->rate);
            echo json_encode($rate);
            break;
        case "user_get":
            $user = getUserDataArray($match["params"]["id"]);
            echo json_encode($user);
            break;
        case "user_fb_get":
            $c = new UserFeedback($match["params"]["id"]);
            $feedbacks = $c->getAllInfo($match["params"]["multiplier"]);
            echo json_encode($feedbacks);
            break;
        case "user_fb_post":
            $postdata = file_get_contents("php://input");
            $request = json_decode($postdata);
            $c = new UserFeedback($match["params"]["id"], $request->rater);
            $rate = $c->addFeedback($request->rate_msg, $request->rate);
            echo json_encode(array($rate));
            break;
        case "user_list_get":
            $list = ProductList::userProducts($match["params"]["id"], $match["params"]["per_page"], $match["params"]["multiplier"]);
            echo json_encode($list);
            break;
        case "list_get":
            $list = new ProductList($match["params"]["id"]);
            $var = $list->getCategoryProducts($match["params"]["multiplier"], $match["params"]["per_page"]);
            if ($var == null)
                echo null;
            else
                echo json_encode($var);
            break;
        case "search":
            $list = new ProductList();
            echo json_encode($list->searchList($match["params"]["query"], $match["params"]["page"]));
            break;
        case "latest":
            $list = new ProductList();
            echo json_encode($list->getLatest($match["params"]["count"]));
            break;
        case "mostSelling":
            $list = new ProductList();
            echo json_encode($list->getMostSelling());
            break;
        case "transpact_update":
            if (isset($_POST['password'])) {
                if ($_POST['password'] == 'af17bc3b4a86a96a0f053a7e5f7c18ba') {
                    if (isset($_POST['transactionID'])) {
                        $transaction_id = $_POST['transactionID'];
                        $transaction_event_id = $_POST['transactionEventID'];
                        if (isset($_POST['description'])) {
                            $description = $_POST['description'];
                        }
                        if (isset($_POST['amount'])) {
                            $amount = $_POST['amount'];
                            Orders::updateTransaction($transaction_event_id, $transaction_id, $amount);
                        } else {
                            Orders::updateTransaction($transaction_event_id, $transaction_id);
                        }
                        echo 1;
                    }
                }
            }
            else
                echo "Invalid call";
            break;
        default:
            echo "Welcome to FlyBuy API.";
            break;
    }

?>