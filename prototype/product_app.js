var app = angular.module('productApp', []);

app.config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
 }]);

app.controller('productController', function($scope, $http) {

    $scope.items = [];
    $scope.selectedItem = 0;
    var currentItem = 0;

    var p = [{data: "[data1]", val : []}, 
             {data: "[data2]", val : [] }];


/* push from prop */

    $scope.addItem = function() {

      $scope.items.push({id:currentItem, prop:p});

      console.log($scope.items);

      $scope.selectedItem = currentItem;
      currentItem = currentItem+1;

    };

    $scope.send = function() {

      var request = $http({
                method: "POST",
                url: "/Libraries/index.php",
                data: {
                    items: $scope.items
                }
            });
        request.success(function(response) {
            
        });
    }

});