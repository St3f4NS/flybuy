var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var debug = true;
var clients = {};

app.get('/', function(req, res) {
  res.send('hello');
});

io.on('connection', function(socket){
	if (debug)
  		console.log('User connected');

  	var id;

  	socket.on('addUser',function(data){
  		id = data.id;
  		if (debug)
  			console.log('User added');
		clients[data.id] = socket.id;
        if (debug)
		    console.log(data.id);
	});

    socket.on('isOnline',function(data){
        if (debug)
            console.log('Checking if user ' + data + ' is online');
        if (clients[data] != undefined)
            var answer = true;
        else
            var answer = false;
        if (debug)
            console.log(answer);
        io.to(clients[id]).emit('isOnline', answer);
    });

    socket.on('addOrder', function(data){
        if (debug)
            console.log('Creating new order for user ' + data);
        if (clients[data] != undefined)
            io.to(clients[data]).emit('newOrder', 1);
    });

	socket.on('send:message', function(data) {
		if (clients[data.u_id] != null)
		{
			var ss = clients[data.u_id];
            if (debug)
			    console.log(data);
			io.to(ss).emit('send:message', {message: data.message, conversation: data.conversation_id});	
		}
	});

	socket.on('disconnect', function(){
		delete clients[id];
		if (debug)
	   		console.log(id + ' user disconnected');
	});
});

app.use(function (req, res, next) {
  res.status(404);
  res.render('404', { url: req.originalUrl });
});

http.listen(3000, function() {
  	console.log('ok');
});