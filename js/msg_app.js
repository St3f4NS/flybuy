

app.factory('socket', function ($rootScope) {
  var socket = io.connect("http://178.62.113.76:3000");
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

app.controller('msg_controller', function($scope, $rootScope, $http, $location, socket) {
	var id = $rootScope.user_id;

    $rootScope.list_of_unique_new = [];
    $rootScope.new_count = 0;
    $rootScope.orders_count = 0;
    $rootScope.sidebarItems = null;

	$scope.addUser = function (user, p) {
		 socket.emit('addUser', {
	      id: user
	    });
        if ($rootScope.user_id == undefined) {
            id = user;
            $rootScope.id = id;
            $scope.loadUser();
        }
	};

	//new message listener
	socket.on('send:message', function (data) {

        var pos = $rootScope.list_of_unique_new.indexOf(data.conversation);

        if (pos == -1) {
            $rootScope.list_of_unique_new.push(data.conversation);
            $rootScope.new_count = $rootScope.list_of_unique_new.length;
        }

	});

    socket.on('newOrder', function(data) {
       $rootScope.orders_count++;
    });

    $scope.getNew = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o : 4,
                u : id
            }
        });

        request.success(function (data) {
            $rootScope.list_of_unique_new = data;
            $rootScope.new_count = $rootScope.list_of_unique_new.length;
        })
    }

    $scope.loadOrdersCount = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 1,
                u_id: id
            }
        });

        request.success(function(data) {
            $rootScope.orders_count = data[0];
        });
    }

    $scope.loadCartCount = function () {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 6,
                u_id: id
            }
        });

        request.success(function(data) {
            $rootScope.cart_count = data[0];
        });
    }
    $scope.loadUser = function () {
        var u = $http.get("/API/user/" + id);
        u.success(function(data) {
            $rootScope.this_user = data;
            if (data['[accType]'] != 1)
                $rootScope.validated = data['[validated]'];
        });
    }

    $scope.loadSidebar = function() {
        var request = $http.get("/API/sidebar");
        request.success(function(data) {
            $rootScope.sidebarItems = data;
        });
    }
$scope.loadSidebar();
});
