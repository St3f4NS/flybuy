var app = angular.module('validations', []);

app.config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
}]);

app.controller('validations', function($scope, $http) {


    $scope.init = function() {
        $scope.loadList();
    }

    $scope.loadList = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/validations_handle.php",
            cache: false,
            data: {
                op: 0
            }
        });
        request.success(function(data) {
            $scope.list = data;
        });
    }

    $scope.select_element = function(id, c, type, index) {

        $scope.selected_method = type;
        $scope.selected_company = c;
        $scope.selected_id = id;
        $scope.selected_index = index;

        $scope.specified = "";
        $scope.proof = "";
        $scope.answer = undefined;
        $scope.answer_msg = "";

        var request = $http({
            method: "POST",
            url: "/Libraries/validations_handle.php",
            cache: false,
            data: {
                op: 1,
                company: c,
                validation: id
            }
        });
        request.success(function(data) {
            $scope.specified = data['submited'];
            $scope.proof = data['proof'];

        });
    }

    $scope.send_answer = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/validations_handle.php",
            cache: false,
            data: {
                op: 2,
                company: $scope.selected_company,
                validation: $scope.selected_id,
                answer: $scope.answer,
                msg: $scope.answer_msg
            }
        });
        request.success(function(data) {
            $scope.list.splice($scope.selected_index, 1);
            $scope.specified = undefined;
            $scope.proof = undefined;
        });

    }
});