app.controller('payment-methods-controller', function($scope, $rootScope, $routeParams, $location, $http) {
    $rootScope.title = "FlyBuy | Payment methods";

    $scope.methods = {
        0: false, //transpact
        1: false, //paypal
        2: false, //wu
        3: false //money gram
    };
    $scope.vals = {
        0: '',
        1: '',
        2: '',
        3: ''
    }

    $scope.init = function() {
        if ($rootScope.logedIn == 0)
            $location.path('/');
        else {
            var u = $http.get("/API/user/" + $rootScope.id);
            u.success(function (data) {
                $scope.user = data;
                var methods = $scope.user['[payment_methods]'];
                angular.forEach(methods, function (o, i) {
                    $scope.methods[i] = true;
                    $scope.vals[i] = o;
                });
            });
        }
    }

    $scope.saveDisabled = function() {
        return ($scope.methods[0] && $scope.vals[0] == '') ||
            ($scope.methods[1] && $scope.vals[1] == '') ||
            ($scope.methods[2] && $scope.vals[2] == '') ||
            ($scope.methods[3] && $scope.vals[3] == '');
    }

    $scope.submit = function() {
        $scope.save_msg = '';
        var request = $http({
            method: "POST",
            url: "/Libraries/payment_method_handler.php",
            data: {
                o: 0,
                u: $rootScope.id,
                m: $scope.methods,
                v: $scope.vals
            }
        });

        request.success(function(data) {
            $location('/user/' + $rootScope.id);
        });
    }
});