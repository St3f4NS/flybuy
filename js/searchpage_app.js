app.controller('search-controller', function($scope, $rootScope, $routeParams, $location, $http) {
    $rootScope.title = "FlyBuy | Search";

    $scope.cloudinary = $rootScope.cloudinary;
    $scope.path = $rootScope.path;

    $scope.search_query = $routeParams.query;
    $scope.page = $routeParams.page_number;

    $scope.query_number = 0;

    $scope.max_pages_displayed = 5;

    $scope.search_data;
    $scope.show_loading = true;

    $scope.$watch('page', function() {
        $scope.searchCall();
    });

    $scope.searchCall = function() {
        var q = "/API/search/" + $scope.search_query + "/" + ($scope.page-1);

        var request = $http.get(q);
        request.success(function(data) {
            $scope.search_data = data[1];
            $scope.query_number = data[0];
            $scope.show_loading = false;

        });
    }

});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});