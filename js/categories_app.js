app.controller('categories-controller', function($scope, $rootScope, $routeParams, $location, $http, $sce) {

    $scope.category_list;
    $scope.items;
    $rootScope.meta_description = "FlyBuy is a place where you can buy and sell your stuff online on a fast and secure way!";
    $rootScope.meta_keywords = "flybuy,buy,sell,online,ecommerce,trade,safe,fast,products";
    $scope.selectedCat = $routeParams.id;
    $scope.page = $routeParams.page_number;
    $scope.haveProducts = false;
    $scope.totalProducts = 0;
    $scope.show_loading = true;
    $rootScope.title = "FlyBuy | Categories";

    $scope.init = function() {
        loadCategories();
        if ($scope.selectedCat == 0)
            loadLatest();
        else
            $scope.setCategory($scope.selectedCat);
    };

    $scope.$watch('selectedCat', function() {
        if ($scope.selectedCat == 0) {
            loadLatest();
            //$location.path("/categories/" + $scope.selectedCat + "/" + $scope.page, false);

        }
    });

    $scope.$watch('page', function() {
        if ($scope.selectedCat != 0) {
            loadCatProducts();
            //$location.path("/categories/" + $scope.selectedCat + "/" + $scope.page, false);
        }
    });


    /*
     <accordion close-others="true">
     <accordion-group ng-if="category.parent_id == 1" heading="{[{ category.name }]}" ng-repeat="category in category_list track by $index">

     <accordion close-others="true">
     <accordion-group ng-repeat="sub in category_list track by $index" ng-if="sub.parent_id == category.id" heading="{[{ sub.name }]}" >
     <div class="panel-body">
     <ul class="child-categories">
     <li ng-repeat="sub_sub in category_list track by $index" ng-show="sub_sub.parent_id == sub.id">
     <a href="" ng-click="setCategory(sub_sub.id);">{[{ sub_sub.name }]}</a>
     </li>
     </ul>
     </div>
     </accordion-group>
     </accordion>
     </accordion-group>
     </accordion>
     */

    var loadCategories = function() {
        $scope.show_loading = true;
        var request = $http({
            method: "POST",
            url: "/Libraries/categories.php",
            data: { op_type: 4 }
        });
        request.success(function(data) {

            $scope.category_list = data;
            $scope.show_loading = false;
        });
    };

    var loadSubCat = function(cat, depth) {
        var req = $http({
            method: "POST",
            url: "/Libraries/categories.php",
            data: {
                op_type: 5,
                parent_id: cat
            }
        });
        req.success(function (data) {
            if (depth == 1) {
                $scope.sub1 = data;
                $scope.loadingSub = false;
            }
            else
                $scope.temp = data;
                $scope.createLinks($scope.temp);
        });
    }

    $scope.getSubCat = function(id) {
        $scope.loadingSub = true;
        loadSubCat(id, 1);
    }
    $scope.selectLinks = function(id) {
        $scope.loadingLinks = true;
        loadSubCat(id, 2);
    }

    /*<div class="panel-body">
     <ul class="child-categories">
     <li ng-repeat="sub_sub in childs track by sub_sub.id">
     <a href="" ng-click="setCategory(sub_sub.id);">{[{ sub_sub.name }]}</a>
     </li>
     </ul>
     </div>*/
    $scope.createLinks = function(links) {
        var body = "<div class='panel-body'><ul class='child-categories'>";
        angular.forEach(links, function(v){
           body += '<li><a href="" ng-click="setCategory(' + v.id +');">' + v.name + "</a></li>";
        });
        body += "</ul></div>";
        $scope.links = $sce.trustAsHtml(body);
        $scope.loadingLinks = false;
    }

    var loadLatest = function () {
        $scope.show_loading = true;

        var request = $http.get('/API/latest/10');
        request.success(function(data) {
            if (data.indexOf("null") == -1)
            {
                $scope.items = data;
                $scope.haveProducts = true;

            }
            else
                $scope.haveProducts = false;
            $scope.show_loading = false;
        })
    };

    $scope.setCategory = function(id) {
        $scope.selectedCat = id;
        //$location.path("/categories/" + id + "/1", false);
        loadCatProducts();
    };

    var loadCatProducts = function() {
        var route = "/API/list/" + $scope.selectedCat + "/" + ($scope.page-1) + "/10";
        $scope.show_loading = true;

        var request = $http.get(route);
        request.success(function(data) {
            $scope.items = data[1];
            $scope.totalProducts = data[0];
            if (data[0] == 0)
                $scope.haveProducts = false;
            else
                $scope.haveProducts = true;
            $scope.show_loading = false;
        });

    }

});
app.directive( 'compileData', function ( $compile ) {
    return {
        scope: true,
        link: function ( scope, element, attrs ) {

            var elmnt;

            attrs.$observe( 'template', function ( myTemplate ) {
                if ( angular.isDefined( myTemplate ) ) {
                    // compile the provided template against the current scope
                    elmnt = $compile( myTemplate )( scope );

                    element.html(""); // dummy "clear"

                    element.append( elmnt );
                }
            });
        }
    };
});
