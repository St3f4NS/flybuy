app.controller('contact-us-controller', function($scope, $rootScope, $http) {
    $rootScope.title = "FlyBuy | Contact us";
    $rootScope.meta_description = "FlyBuy is a place where you can buy and sell your stuff online on a fast and secure way!";
    $rootScope.meta_keywords = "flybuy,buy,sell,online,ecommerce,trade,safe,fast,products";

    $scope.sendMail = function () {
        $scope.status = "";
        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o : 7,
                subject : $scope.subject,
                message : $scope.msg,
                mail : $scope.mail,
                name : $scope.sender
            }
        });
        request.success(function(data) {
            $scope.status = "Your message has been sent.";
        });
    }
});