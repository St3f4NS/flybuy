app.controller('search_bar', function($scope, $location) {
    $scope.searchTypes = ["Products"/*, "Companies"*/];
    $scope.searchQuery = "";

    $scope.selectedType = $scope.searchTypes[0];

    $scope.selectType = function (type) {
        $scope.selectedType = type;
    }

    $scope.search = function() {
        if ($scope.searchQuery != "") {
            $location.path('/search/' + $scope.searchQuery + '/1');
        }

    }

})