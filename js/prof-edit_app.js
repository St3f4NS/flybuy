app.controller('prof-edit-controller', function($scope, $rootScope, $routeParams, $location, $http) {
    $scope.country_id = 0;
    $scope.country_name = "Select country";
    $scope.valid_c_select = -1;
    $scope.valid_c_name = "Select country";
    $scope.validationMethod_name = "Select method of validation";
    $scope.validationMethod_id = -1;
    $scope.password = '';
    $scope.vat_country = [
        { value: 'AT', label: 'AT-Austria' },
        { value: 'BE', label: 'BE-Belgium' },
        { value: 'BG', label: 'BG-Bulgaria' },
        { value: 'CY', label: 'CY-Cyprus' },
        { value: 'CZ', label: 'CZ-Czech Republic' },
        { value: 'DE', label: 'DE-Germany' },
        { value: 'DK', label: 'DK-Denmark' },
        { value: 'EE', label: 'EE-Estonia' },
        { value: 'EL', label: 'EL-Greece' },
        { value: 'ES', label: 'ES-Spain' },
        { value: 'FI', label: 'FI-Finland' },
        { value: 'FR', label: 'FR-France ' },
        { value: 'GB', label: 'GB-United Kingdom' },
        { value: 'HR', label: 'HR-Croatia' },
        { value: 'HU', label: 'HU-Hungary' },
        { value: 'IE', label: 'IE-Ireland' },
        { value: 'IT', label: 'IT-Italy' },
        { value: 'LT', label: 'LT-Lithuania' },
        { value: 'LU', label: 'LU-Luxembourg' },
        { value: 'LV', label: 'LV-Latvia' },
        { value: 'MT', label: 'MT-Malta' },
        { value: 'NL', label: 'NL-The Netherlands' },
        { value: 'PL', label: 'PL-Poland' },
        { value: 'PT', label: 'PT-Portugal' },
        { value: 'RO', label: 'RO-Romania' },
        { value: 'SE', label: 'SE-Sweden' },
        { value: 'SI', label: 'SI-Slovenia' },
        { value: 'SK', label: 'SK-Slovakia' },
        { value: 'PK', label: 'PK-Pakistan' },
        { value: 'IN', label: 'IN-India'}
    ];
    $scope.validationMethods = [
        { id: 0, name: 'VAT validation' },
        { id: 1, name: 'Proof upload' }
    ];

    $scope.init = function() {
        if ($rootScope.id == undefined)
            $location.path('/');

        var request = $http.get("/API/user/" + $rootScope.id);
        request.success(function(data) {
           $scope.user_data = data;
            $scope.country_id = data['[country_id]'];
            $scope.country_name = data['[country]'];
        });
        loadC();
    }

    $scope.setC = function(id, name) {
        $scope.country_id = id;
        $scope.country_name = name;
        angular.element('#vatC').val(id);
    }

    var loadC = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/location_handle.php",
            data: {
                op: 1
            }
        });
        request.success(function(data) {
            $scope.counteries = data;
        });
    }

    $scope.set_valid_c = function(id, name) {
        $scope.valid_c_select = id;
        $scope.valid_c_name = name;
        angular.element('#vatC').val(id);
    }

    $scope.setValidMethod = function (id, name) {
        $scope.validationMethod_name = name;
        $scope.validationMethod_id = id;
        angular.element('#v_method').val(id);
    }

    $scope.validate = function() {
        $scope.vat_res = "";
        if ($scope.valid_c_select == null || $scope.valid_c_select == undefined || $scope.valid_c_select == "")
            $scope.vat_res = "Please fill all the data.";
        else
        {
            var request = $http({
                method: "POST",
                url: "/Libraries/register_form_details.php",
                data: {
                    country: $scope.valid_c_select,
                    vat: $scope.vat
                }
            });
            $scope.loading = true;
            request.success(function(data) {
                $scope.loading = false;
                if (data == 1)
                    $scope.vat_res = "VAT is valid.";
                else
                    $scope.vat_res = "VAT is not valid.";
                $scope.validated = true;
            });
        }
    }

    $scope.setImgUploaded = function () {
        $scope.img_uploaded = true;
        console.log(1);
    }

    $scope.$watch('password', function() {
       if ($scope.password != '')
            $scope.passOk = false;
       else
           $scope.passOk = true;
    });

    $scope.valid_form = function() {
        var v = !(($scope.update_form.$valid || $scope.passOk)
        && (!$scope.enable_validation ||
        ($scope.enable_validation && $scope.validationMethod_id == 0 && $scope.validated) ||
        ($scope.enable_validation && $scope.validationMethod_id == 1 && $scope.img_uploaded)));
        return v;
    }

});
