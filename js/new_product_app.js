app.config(['$tooltipProvider', function($tooltipProvider){
    $tooltipProvider.setTriggers({
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur',
        'never': 'mouseleave' // <- This ensures the tooltip will go away on mouseleave
    });
}]);
app.directive('loader', function () {
    return {
        link: function (scope, element, attrs) {
           scope.loadTag();
        }
    }
});

app.controller('new-product-controller', function($scope, $rootScope, $routeParams, $location, $http, $compile, $timeout) {
    $rootScope.title = "FlyBuy | New product";
    $scope.currencies = ['GBP', 'EUR', 'USD'];
    $scope.currency = $scope.currencies[0];

	$scope.category1 = [];
	$scope.discounts = [];
	$scope.calculations = [];
	$scope.discountType = ['%', '00'];
	$scope.image_ids = [];
	$scope.shipping = [];
    var newVars = [];
	$scope.fields = [];

	$scope.variations = [];
	$scope.selectedVariation = 0;
	var varNumber = 0;
	var current;
    var update = false;
    $scope.loader = false;

    $scope.loadTag = function () {
        var tag = $rootScope.img_tag;
        var elmnt = $compile(tag)($scope);
        $('.img-ul').html(elmnt);

        $('.cloudinary-fileupload').bind('fileuploadstart', function(e) {
            $('.status').html('Uploading image...');
        });
        $('.cloudinary-fileupload').bind('fileuploadfail', function(e) {
            $('.status').html("Image upload failed.");
        });

// Upload finished
        $('.cloudinary-fileupload').bind('cloudinarydone', function(e, data) {
            $('.status').html('');
            $('.preview').append(
                $.cloudinary.image(data.result.public_id,
                    {  format: data.result.format, version: data.result.version,
                        crop: 'scale', width: 200, id: 'prev', class: data.result.public_id }));
            $('.image_public_id').val(data.result.public_id);
            $scope.addImg(data.result.public_id);
        });

        $(document).ready(function() {
            $('.preview').on('click', 'img', function (e) {
                var id = $(this).attr('class');
                $scope.removeImage(id);
                $(this).remove();
            });
        });
    }
	$scope.init = function() {
		$scope.loadCategories();
		$scope.addDiscount();
	}

    $scope.setCategory = function(id, name) {
        $scope.c1 = id;
        $scope.c1_name = name;
    }

    $scope.setSubCat = function(id, name) {
        $scope.c2 = id;
        $scope.c2_name = name;
    }

    $scope.setMainCat = function(id, name) {
        $scope.category = id;
        $scope.category_name = name;
    }

    $scope.setCurrency = function(id) {
        $scope.currency = $scope.currencies[id];
    }

	$scope.loadCategories = function() {
		var request = $http({
                method: "POST",
				url: "/Libraries/new_product_handler.php",
                data: {
                    op: 1
                }
            });
		request.success(function(data) {
			$scope.category1 = data;
		});
	}

	$scope.addDiscount = function() {
		var disc = $scope.discountType[0];

		$scope.discounts.push({discount_value: '', discount_type: disc, discount_quantity: ''});
	}

    $scope.removeDiscount = function(index) {
        $scope.discounts.splice(index, 1);
    }

	$scope.send = function() {
        $scope.loader = true;
		var data = { name: $scope.productName,
					price: $scope.price,
					currency: $scope.currency,
					quantity: $scope.quantity,
					category: $scope.category,
					description: $scope.description,
					discounts: $scope.discounts,
					pictures: $scope.image_ids,
					variations: $scope.variations };

		var request = $http({
                method: "POST",
				url: "/Libraries/new_product_handler.php",
                data: {
                    op: 2,
                    product: data
                }
            });
		request.success(function(data) {
            $scope.loader = false;
		});
	}

	$scope.calculateDiscount = function(val, type, quantity) {

		if (type == "%")
			return ($scope.price*quantity-$scope.price*quantity*(val/100));
		else
			return ($scope.price*quantity - val);
	}


	$scope.addImg = function(id) {
		$scope.image_ids.push(id);
	}

	$scope.removeImage = function(id) {
        var index = $scope.image_ids.indexOf(id);
		$scope.image_ids.splice(index, 1);
	}

	$scope.$watch('c1', function() {
		$scope.fields = [];
		$scope.variations = [];
		varNumber = 0;
		$scope.c2 = undefined;
        $scope.c2_name = undefined;
	});

	$scope.$watch('c2', function() {
		$scope.fields = [];
		$scope.variations = [];
		varNumber = 0;
		$scope.category = undefined;
        $scope.category_name = undefined;
	});

	$scope.$watch('category', function() {
        $scope.variation_loaded = false;
		if ($scope.category != undefined && current != $scope.category)
		{
	    	var request = $http({
	                method: "POST",
					url: "/Libraries/categories.php",
	                data: {
	                    op_type: 2,
	                    category_id: $scope.category
	                }
	            });	
	    	request.success(function(data) {
				$scope.fields = data;
                if (!update) {
                    $scope.variations = [];
                    varNumber = 0;
                    $scope.addVariation();
                }
				current = $scope.category;
                $scope.variation_loaded = true;
			});	
	    }
   	});

   	$scope.addVariation = function() {
   		$scope.variations.push({});
   		$scope.selectedVariation = varNumber;
        newVars.push(varNumber);
   		varNumber++;
   	}

    $scope.setSelectedVariation = function(index) {
        $scope.selectedVariation = index;
    }

    $scope.loadProduct = function() {
        update = true;
        $rootScope.title = "FlyBuy | Update product";
        $scope.product_id = $routeParams.id;
        $scope.pName = '';
        var request = $http.get("/API/product/" + $scope.product_id);
        request.success(function(data) {
            $scope.owner = data['user_id'];
            if ($scope.owner != $rootScope.id)
                $location.path('/');
            else {
                $scope.category = data.category_id;
                $scope.product = data;
                $scope.pName = data.name;
                $scope.discounts = data.discounts;
                $scope.currency = data.currency;
                $scope.available = data['meta'][1]['quantity'];
                $scope.description = data['meta'][0]['description'];
                $scope.price = $scope.product['price'];


                angular.forEach(data.variations, function (val, key) {
                    var obj = {};
                    obj['id'] = key;

                    angular.forEach(val, function (d, v) {
                        obj[d['data']] = d['value'];
                    });
                    $scope.variations.push(obj);
                    varNumber++;
                });
                var imgs = data.pictures;

                for (var i = 0; i < imgs.length; i++) {
                    $scope.addImg(imgs[i]);
                }
            }
        });
    }

    $scope.update = function() {
        var data = {
            id: $scope.product_id,
            price: $scope.price,
            currency: $scope.currency,
            quantity: $scope.available,
            description: $scope.description,
            discounts: $scope.discounts,
            pictures: $scope.image_ids,
            variations: $scope.variations,
            name: $scope.pName };

        var request = $http({
            method: "POST",
            url: "/Libraries/new_product_handler.php",
            data: {
                op: 3,
                d: data
            }
        });
        request.success(function(data) {
            $location.path('/product/' + $scope.product_id);
        });
    }


});
