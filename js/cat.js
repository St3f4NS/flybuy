var app = angular.module('cat', []);

app.config(['$interpolateProvider', function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
 }]);

app.controller('ctrl', function($scope, $http) {
    $scope.fields = {};
    $scope.fieldKeys = [];

    $scope.setFieldKeys = function() {
        var keys = [];
        for (key in $scope.fields) {
            keys.push(key);
        }
        $scope.fieldKeys = keys;
    }

    $scope.clearData = function() {
        $scope.fields = {};
        $scope.fieldKeys = [];
    }

    $scope.removeField = function(id) {

        var category_id = document.getElementById('modal_id').value;

        var request = $http({
                method: "POST",
                url: "/Libraries/categories.php",
                data: {
                    op_type: 3,
                    category_id: category_id,
                    data: $scope.fieldKeys[id]
                }
            });
        request.success(function(response) {
            if (response != -1)
                $scope.fieldKeys.splice(id, 1);
        });
    }

    $scope.loadFields = function(id) {
        var request = $http({
                method: "POST",
                url: "/Libraries/categories.php",
                data: {
                    op_type: 2,
                    category_id: id
                }
            });
        request.success(function(response) {
            if (response != -1)
            {
                angular.forEach(response, function(d) {
                    $scope.fields[d.key] = d.val;
                    $scope.setFieldKeys();
                });
                
            }
        });
    }
    
    $scope.addField = function() {
        var category_id = document.getElementById('modal_id').value;

        var request = $http({
                method: "POST",
                url: "/Libraries/categories.php",
                data: {
                    op_type: 1,
                    category_id: category_id,
                    data: $scope.keyToAdd,
                    value: $scope.valueToAdd
                }
            });
        request.success(function(data) {
            if (data == 1)
            {
                $scope.fields[$scope.keyToAdd] = $scope.valueToAdd;
                $scope.setFieldKeys();
                $scope.keyToAdd = '';
                $scope.valueToAdd = '';
            }
        });
        
    }
    $scope.setFieldKeys();
});



    function modal(id, parent)
    {
        document.getElementById('modal_id').value = id;
        document.getElementById('newname').value= '';
        angular.element($("#controller")).scope().loadFields(id);
        document.getElementById('updateParent').value = parent;
        $('#editor').modal('show');
        
    }

    function callFunction(category)
    {
            var func = "remove";

            $.ajax({
                type: "POST",
                url: "/Libraries/categories.php",
                data: { operation : func, id : category } 
            });
            removeSelect(category);
    };
    function removeSelect(id)
    {
        $("#selectParent option[value='" + id + "']").remove();  
    };