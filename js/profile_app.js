app.config(['$tooltipProvider', function($tooltipProvider){
    $tooltipProvider.setTriggers({
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur',
        'never': 'mouseleave'
    });
}]);


app.controller('profile-controller', function($scope, $rootScope, $routeParams, $location, $http, socket, $compile, $timeout) {
    $rootScope.title = "FlyBuy | Profile";

    var current_id = $rootScope.id;
    $scope.viewing_id = $routeParams.id;
    $scope.user_info;
    var accType;
    $scope.myAccount = ($scope.viewing_id == current_id);
    $scope.hasTranspact;
    $scope.rate = 3;
    $scope.ratingText = ''
    var back_link = null;
    var single_order_id = null;
    $scope.img_loading = false;
    $scope.table_data = [];
    $scope.showBadges = false;
    $scope.code_disabled = false;

    $scope.init = function() {
        getUser();

        if (current_id == undefined)
            $scope.logedIn = false;

        if ($routeParams.order_id != undefined)
            single_order_id = $routeParams.order_id;

        if (!$scope.myAccount)
            checkOnline();

    }

    $scope.showBadges = function() {
        $scope.showBadges = (($scope.user_info['[validated]'] == 1) || ($scope.user_info['[rate]'] == 5) || ($scope.user_info['[join_years]'] > 0));
    }

    $scope.clickImg = function() {
        $timeout(function() {
            $('.cloudinary-fileupload').click();
        }, 0);
    }

    $scope.loadTag = function () {
        var tag = $rootScope.img_tag;
        var elmnt = $compile(tag)($scope);
        $('.img-ul').html(elmnt);
        $('.cloudinary-fileupload').bind('fileuploadstart', function(e) {
            $scope.img_loading = true;
        });
        $('.cloudinary-fileupload').bind('fileuploadfail', function(e) {
            $scope.img_loading = false;

        });

// Upload finished
        $('.cloudinary-fileupload').bind('cloudinarydone', function(e, data) {
            setProf(data.result.public_id, data.result.version);
        });
    }
/*

    $('.status').html('');
    $('.preview').append(
        $.cloudinary.image(data.result.public_id,
            {  format: data.result.format, version: data.result.version,
                crop: 'scale', width: 200, id: 'prev', class: data.result.public_id }));
    $('.image_public_id').val(data.result.public_id);
    angular.element('#new_prod').scope().addImg(data.result.public_id);
    return true;*/



    $scope.$watch('ratingText', function() {
        $scope.textLength = $scope.ratingText.length;
    });

    var getUser = function() {
        var request = $http.get('/API/user/' + $scope.viewing_id);
        request.success(function(data) {
            $scope.user_info = data;
            $scope.hasPaymentm = data['[payment_methods]'].length;
            accType = data['[accType]'];
            $scope.showBadges();
            if($routeParams.page != null)
                $scope.setTab($routeParams.page);
            else
                $scope.setTab("reviews");
        })
    }

    var setProf = function(img, vers) {
        var request = $http({
            method: "POST",
            url: "/Libraries/image_handler.php",
            data: {
                u: current_id,
                i: img,
                v: vers
            }
        });

        request.success(function(data) {
            $scope.user_info['[img_ver]'] = vers;
            $scope.img_loading = false;
        });
    }

    $scope.setTab = function(page) {
        if ($scope.selected_tab != page) {
            $scope.selected_tab = page;
            switch (page){
                case "products":
                    loadResource(0, 10);
                    break;
                case "orders":
                    if (accType != 1 && $scope.myAccount) {
                        getOrders(0);
                        $scope.current_pagination_orders = 1;
                    }
                    else
                        $scope.setTab("reviews");
                    break;
                case "purchase":
                    if (accType != 0 && $scope.myAccount) {
                        getOrders(2);
                        $scope.current_pagination_purchase = 1;
                    }
                    else
                        $scope.setTab("reviews");
                    break;
                case "reviews":
                    loadResource(0, null);
                    break;
                case "single_order":
                    if (!$scope.myAccount || single_order_id == null)
                       $scope.setTab("reviews");
                    else
                        loadOrder();
                    break;
                default:
                    $scope.setTab("reviews");
                    break;

            }
            //location.skipReload().path("/user/" + $scope.viewing_id + "/" + page);
            //$location.path("/user/" + $scope.viewing_id + "/" + page);
            $scope.rated_answer = "";
        }
    }

    $scope.changePage = function() {
        switch ($scope.selected_tab){
            case "products":
                loadResource($scope.current_pagination_products-1, 10);
                break;
            case "reviews":
                loadResource($scope.current_pagination_reviews-1, null);
                break;
        }
    }

    var loadResource = function(param1, param2) {
        $scope.loader = true;
        var params = '';
        if (param1 != null)
            params = params + '/' + param1;
        if (param2 != null)
            params = params + '/' + param2;

        $scope.table_data = null;
        $scope.additional = null;
        if ($scope.selected_tab == 'products')
            var request = $http.get('/API/user/' + $scope.user_info['[company_id]'] + '/' + $scope.selected_tab + params);
        else
            var request = $http.get('/API/user/' + $scope.viewing_id + '/' + $scope.selected_tab + params);
        request.success(function(data){
            $scope.count = data[0];
            $scope.table_data = data[1];
            if (data[2] != undefined)
                $scope.additional = data[2];
            $scope.loader = false;
        });
    }

    var checkOnline = function() {
        socket.emit('isOnline', $scope.viewing_id);
    }

    socket.on('isOnline', function (data) {
        $scope.onlineStatus = data;
    });

    $scope.send_message_to_id = function() {
        $scope.stat_msg = "";
        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o : 6,
                u2: 'new_msg',
                t: $scope.new_msg_txt,
                u: current_id,
                u2_id: $scope.viewing_id
            }
        });

        request.success(function(data) {
            if (data == "Error while sending message." || data == "Unable to find specified user.")
                $scope.stat_msg = data;
            else {
                $scope.stat_msg = "Message sent.";
                var sending_data = { u_id: $scope.viewing_id, message: data, conversation_id: -1 };
                socket.emit('send:message', sending_data);
            }
        });
    }

    $scope.rateUser = function() {

        var request = $http({
            method: "POST",
            url: "/API/user/" + $scope.viewing_id + "/reviews",
            data: {
                rater: current_id,
                rate_msg: $scope.ratingText,
                rate: $scope.rate
            }
        });
        request.success(function(data) {
            if (data[0] != -1) {
                $scope.table_data.unshift(data);
                $scope.count++;
                $scope.rated_answer = "Raiting added";
            }
            else
                $scope.rated_answer = "You have already rated this user.";

        });
    }

    var getOrders = function(operation) {
        $scope.loading = true;
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o : operation,
                u_id: $scope.viewing_id
            }
        });
        request.success(function(data) {
            $scope.table_data = data;
            $scope.count = data.length;
            $scope.loading = false;
        })
    }

    $scope.single_order_tab = function(order_id, back_loc) {
        single_order_id = order_id;
        back_link = back_loc;
        $scope.setTab("single_order");
    }

    var loadOrder = function () {
        $scope.loading = true;
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 3,
                u_id: $scope.viewing_id,
                order: single_order_id
            }
        });

        request.success(function(data) {
            $scope.single_order = data;
            if ($scope.single_order.transaction_id != undefined) {
                $scope.payment_code = $scope.single_order.transaction_id;
                $scope.code_disabled = true;
            }
            $scope.loading = false;
        });
    }

    $scope.goBack = function() {
        if (back_link != null)
            $scope.setTab(back_link);
        else
            $scope.setTab("reviews");
    }

    $scope.total = function(prod, shipping) {
        return (parseInt(prod) + parseInt(shipping));
    }

    $scope.approveOrder = function() {
        $scope.approve_status = "";
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 5,
                u_id: $scope.viewing_id,
                order_id: single_order_id,
                replay: 1,
                payment_method: $scope.single_order.payment_method,
                shipping_price: $scope.shipping_price,
                user_name: $scope.single_order.buyer_firstname + " " + $scope.single_order.buyer_lastname,
                buyer_mail: $scope.single_order.buyer_data['[email]'],
                buyer_id: $scope.single_order.buyer_data['[id]'],
                seller_mail: $scope.user_info['[email]'],
                price: $scope.single_order.price,
                currency: $scope.single_order.product_currency
            }
        });
        request.success(function(data){
            if (data.indexOf('-1') == -1) {
                $scope.single_order.status = 6;
                $scope.single_order.shipping = $scope.shipping_price;
                $scope.approve_status = "Order approved.";
            }
            else
                $scope.approve_status = "Error.";
        });
    }
    //$request->order, $request->code
    $scope.saveCode = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 12,
                u_id: $scope.viewing_id,
                order: single_order_id,
                code: $scope.payment_code,
                s_mail: $scope.single_order.seller_email,
                s_id: $scope.single_order.seller_id
            }
        });
        request.success(function(data){
            $scope.code_disabled = true;
        });
    }

    $scope.dissaproveOrder = function(note) {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 5,
                u_id: $scope.viewing_id,
                order_id: single_order_id,
                replay: 2,
                note: note,
                user_name: $scope.single_order.buyer_firstname + " " + $scope.single_order.buyer_lastname,
                buyer_mail: $scope.single_order.buyer_data['[email]']
            }
        });
        request.success(function(data){
            if (data.indexOf('-1') == -1) {
                $scope.decline_status = "Order disapproved";
                $scope.single_order.status = 2;
            }
            else
                $scope.decline_status = "Error.";

        });
    }

    $scope.subscriptionEndDate = function () {
        var request = $http({
            method: "POST",
            url: "/Libraries/paypal_handler.php",
            data: {
                user: $scope.viewing_id
            }
        });
        request.success(function(data) {
            if (data.indexOf(-1) == -1)
                $scope.subscription_info = "Subscription end date: " + data;
            else
                $scope.subscription_info = "You havent paid for your subscription yet.";
        });
    }

});