app.controller('cart-controller', function($scope, $rootScope, $routeParams, $location, $http) {
    $rootScope.meta_description = "FlyBuy is a place where you can buy and sell your stuff online on a fast and secure way!";
    $rootScope.meta_keywords = "flybuy,buy,sell,online,ecommerce,trade,safe,fast,products";

    var id = $rootScope.id;
    $scope.haveProducts = false;
    $scope.totalProducts = 0;
    $rootScope.title = "FlyBuy | Cart";
    $scope.products = []
    $scope.current_pagination = 1;
    $scope.total = 0;

    $scope.init = function() {
        getList();
    }

    $scope.totalCalc = function(amount) {
        $scope.total += parseInt(amount);
    }

    var getList = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 7,
                u_id: id
            }
        });
        request.success(function(data){
            var length = data.length;
            for (var i = 0; i < length; i++) {
                getProduct(data[i]);
            }
        });

    }

    var getProduct = function(product) {
        var prod = $http.get("/API/product/" + product);
        prod.success(function(data){
            $scope.products.push(data);
        });
    }

    $scope.removeItem = function(product, index) {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 8,
                u_id: id,
                product: product
            }
        });
        request.success(function(data){
            $scope.products.splice( index, 1 );
            $rootScope.cart_count--;
        });
    }

    $scope.emptyCary = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 9,
                u_id: id
            }
        });
        request.success(function(data){
            $scope.products = [];
            $rootScope.cart_count = 0;
        });
    }
});