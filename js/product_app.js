app.controller('single_product-controller', function($scope, $rootScope, $routeParams, $location, $http, socket) {
    $rootScope.title = "FlyBuy | Product";
    $rootScope.meta_description = "FlyBuy is a place where you can buy and sell your stuff online on a fast and secure way!";
    $rootScope.meta_keywords = "flybuy,buy,sell,online,ecommerce,trade,safe,fast,products";

    var current_id = $rootScope.id;
    $scope.product_id = $routeParams.id;
    $scope.product_data = null;
    $scope.images = null;
    $scope.selectedImage = null;
    $scope.totalAvailable = 0;
    $scope.discounts = null;
    $scope.checkoutPrice = 0;
    $scope.selectedVariation = 0;
    $scope.reviewsCount = 0;
    $scope.msg_length = 0;
    $scope.submitReviewMsg = "";
    $scope.rate = 3;
    $scope.current_pagination_reviews = 1;
    $scope.buy_notes = '';
    $rootScope.contentUrl = "product/" + $scope.product_id;
    $scope.inCart = false;
    $scope.support_payment_methods = '';
    $scope.payment_methods_list = [];

    $scope.init = function() {
        getProduct();
        $scope.setTab('description');
    }

    var getProduct = function() {
        var api_call = "/API/product/" + $scope.product_id;
        var request = $http.get(api_call);

        request.success(function(data) {
            $scope.product_data = data;
            $rootScope.title = "FlyBuy | " + data['name'];
            $scope.images = data['pictures'];
            $scope.selectedImage = $scope.images[0];
            $scope.totalAvailable = data['meta'][1]['quantity'];
            $rootScope.meta_description = data['meta'][0]['description'];
            $scope.discounts = data['discounts'];
            $scope.variations = data['variations'];
            createPaymentMethods();
            createKeywords(data['name']);
        });
    }

    $scope.selectImage = function(newImg) {
        if ($scope.selectedImage != newImg)
            $scope.selectedImage = newImg;
    }

    $scope.priceCalculator = function() {
        var max = -1; //buy-quantity

        if ($scope.discounts != null) {
            for (var i = 0; i < $scope.discounts.length; i++)
            {
                if ($scope.discounts[i].discount_quantity <= $scope.buy_quantity) {
                    if (max == -1)
                        max = i;
                    else
                        if ($scope.discounts[i].discount_quantity > $scope.discounts[max].discount_quantity)
                            max = i;

                }
            }
            if (max != -1) {
                var disc_type = $scope.discounts[max].discount_type;
                var disc_value = $scope.discounts[max].discount_value;

                if (disc_type == "%")
                    $scope.checkoutPrice = $scope.product_data['price'] * $scope.buy_quantity - $scope.product_data['price'] * $scope.buy_quantity * (disc_value / 100);
                else
                    $scope.checkoutPrice = $scope.product_data['price'] * $scope.buy_quantity - disc_value;
            }
            else
                $scope.checkoutPrice = $scope.product_data['price'] * $scope.buy_quantity;
        }
        else
            $scope.checkoutPrice = $scope.product_data['price'] * $scope.buy_quantity;

    }

    $scope.changePage = function() {
        getReviews();
    }

    var getReviews = function() {
        $scope.loading = true;
        var api_call = "/API/product/" + $scope.product_id + "/reviews/" + ($scope.current_pagination_reviews -1);
        var request = $http.get(api_call);
        request.success(function(data) {
            $scope.reviews = data[1];
            $scope.reviewsCount = data[0];
            $scope.loading = false;
        });
    }

    $scope.$watch('submitReviewMsg', function() {
        $scope.textLength = $scope.submitReviewMsg.length;
    });

    $scope.submitReview = function() {
        $scope.rated_answer = "";
        var request = $http({
            method: "POST",
            url: "/API/product/" + $scope.product_id  + "/reviews",
            data: {
                rater: current_id,
                rate_msg: $scope.submitReviewMsg,
                rate: $scope.rate
            }
        });
        request.success(function(data) {
            if (data.indexOf('-1') == -1) {
                $scope.reviews.unshift(data[0]);
                $scope.reviewsCount++;
                $scope.rated_answer = "Raiting added";
            }
            else
                $scope.rated_answer = "You have already rated this user.";

        });
    }

    $scope.createOrder = function () {
        $scope.newOrderAnswer = '';
        var data = [];
        data[0] = $scope.product_data['id'];
        data[1] = $scope.variation_id;
        data[2] = $scope.buy_quantity;
        data[3] = $scope.checkoutPrice;
        data[4] = $scope.product_data['user_id'];
        data[5] = 0;
        data[6] = '';
        data[7] = $scope.buy_notes;
        data[8] = $scope.payment_method;

        var request = $http({
            method: "POST",
            url: "Libraries/orders_handler.php",
            data: {
                o: 4,
                u_id: current_id,
                order_data: data
            }
        });
        request.success(function(data){
            if (data.indexOf('-1') == -1) {
                $scope.newOrderAnswer = "New order created. Waiting for supplier's approval.";
                socket.emit('addOrder', $scope.product_data['user_id']);
            }
            else
                $scope.newOrderAnswer = "Error creating new order.";
        });
    }

    $scope.setTab = function(tab) {
        if (tab != $scope.selectedTab)
            $scope.selectedTab = tab;
        if (tab == 'feedback')
            getReviews();
    }

    $scope.selectVariationPreview = function(index) {
        if ($scope.selectedVariation != index)
            $scope.selectedVariation = index;
    }

    $scope.selectVariation = function(selected, id) {
        if ($scope.buy_variation != selected)
            $scope.buy_variation = selected;
        $scope.variation_id = id;
    }

    $scope.checkAvailable = function() {
        var totalAvailable = parseInt($scope.totalAvailable);
        var buy_quantity = parseInt($scope.buy_quantity);
        return (totalAvailable < buy_quantity);
    }

    var createKeywords = function(word) {
        var newWord = word.replace(' ', ',');
        $rootScope.meta_keywords = $rootScope.meta_keywords + "," + newWord;
    }

    $scope.addToCart = function() {
            var request = $http({
                method: "POST",
                url: "/Libraries/orders_handler.php",
                data: {
                    o: 10,
                    u_id: current_id,
                    product: $scope.product_id
                }
            });
            request.success(function(data){
                if (data.indexOf('-1') == -1)
                    $rootScope.cart_count++;
                $scope.inCart = true;
            });
        }

    var createPaymentMethods = function () {
        var method_id = $scope.product_data.payment_methods;
        var temp = ''
        if ($rootScope.this_user != undefined) {
            var array1 = $rootScope.this_user['[payment_methods]'];
            var common = [];
            var result = [];
            angular.forEach(method_id, function (o, i) {
                if (typeof array1[i] !== 'undefined') {
                    common.push(i);
                }
            });

            angular.forEach(common, function (i) {
                var m_name;
                m_name = getMethodName(parseInt(i));
                result.push({id: i, name: m_name});
            });

            $scope.payment_methods_list = result;
            if ( $scope.payment_methods_list.length != 0)
                $scope.payment_method = $scope.payment_methods_list[0].id;
        }

            angular.forEach(method_id, function (o, i) {
                var m_name = getMethodName(parseInt(i));

                if (temp == '')
                    temp += m_name;
                else
                    temp += ", " + m_name;
            });

        $scope.supported_payment_methods = temp;
    }

    var getMethodName = function (i) {
        switch (i) {
            case 0:
                return "Transpact";
                break;
            case 1:
                return "PayPal";
                break;
            case 2:
                return "Western Union";
                break;
            case 3:
                return "MoneyGram";
                break;
        }
    }
});
