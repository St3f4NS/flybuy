angular.module('validation.match', []);

angular.module('validation.match').directive('match', match);

function match ($parse) {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, elem, attrs, ctrl) {
            if(!ctrl) {
                if(console && console.warn){
                    console.warn('Match validation requires ngModel to be on the element');
                }
                return;
            }

            var matchGetter = $parse(attrs.match);

            scope.$watch(getMatchValue, function(){
                ctrl.$validate();

            });

            ctrl.$validators.match = function(){
                return ctrl.$viewValue === getMatchValue();
            };

            function getMatchValue(){
                var match = matchGetter(scope);
                if(angular.isObject(match) && match.hasOwnProperty('$viewValue')){
                    match = match.$viewValue;
                }

                return match;
            }
        }
    };
}

app.directive('loader', function () {
    return {
        link: function (scope, element, attrs) {
            scope.loadTag();
        }
    }
});

app.config(['$tooltipProvider', function($tooltipProvider){
    $tooltipProvider.setTriggers({
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur',
        'never': 'mouseleave' // <- This ensures the tooltip will go away on mouseleave
    });
}]);

app.controller('reg', function($scope, $rootScope, $routeParams, $http, $compile, $timeout) {

    $rootScope.title = "FlyBuy | Register";
    $scope.validationMethods = [
        { id: 0, name: 'VAT validation' },
        { id: 1, name: 'Proof upload' }
    ];
	$scope.vat_country = [
	            { value: 'AT', label: 'AT-Austria' },
	            { value: 'BE', label: 'BE-Belgium' },
	            { value: 'BG', label: 'BG-Bulgaria' },
	            { value: 'CY', label: 'CY-Cyprus' },
	            { value: 'CZ', label: 'CZ-Czech Republic' },
	            { value: 'DE', label: 'DE-Germany' },
	            { value: 'DK', label: 'DK-Denmark' },
	            { value: 'EE', label: 'EE-Estonia' },
	            { value: 'EL', label: 'EL-Greece' },
	            { value: 'ES', label: 'ES-Spain' },
	            { value: 'FI', label: 'FI-Finland' },
	            { value: 'FR', label: 'FR-France ' },
	            { value: 'GB', label: 'GB-United Kingdom' },
	            { value: 'HR', label: 'HR-Croatia' },
	            { value: 'HU', label: 'HU-Hungary' },
	            { value: 'IE', label: 'IE-Ireland' },
	            { value: 'IT', label: 'IT-Italy' },
	            { value: 'LT', label: 'LT-Lithuania' },
	            { value: 'LU', label: 'LU-Luxembourg' },
	            { value: 'LV', label: 'LV-Latvia' },
	            { value: 'MT', label: 'MT-Malta' },
	            { value: 'NL', label: 'NL-The Netherlands' },
	            { value: 'PL', label: 'PL-Poland' },
	            { value: 'PT', label: 'PT-Portugal' },
	            { value: 'RO', label: 'RO-Romania' },
	            { value: 'SE', label: 'SE-Sweden' },
	            { value: 'SI', label: 'SI-Slovenia' },
	            { value: 'SK', label: 'SK-Slovakia' },
                { value: 'PK', label: 'PK-Pakistan' },
                { value: 'IN', label: 'IN-India' },
                { value: 'PH', label: 'Philippines'}
	];

    $scope.errormsg = '';
    if ($routeParams.error != undefined) {
        switch ($routeParams.error) {
            case 'exist':
                $scope.errormsg = "Account already exists.";
                break;
            case 'mail':
                $scope.errormsg = "Please input valid Email form.";
                break;
            case 'failed':
                $scope.errormsg = "Failed to register.";
                break;
            case 'company_exist':
                $scope.errormsg = "That company already exist.";
                break;
            default:
                $scope.errormsg = "";
        }
    }

	$scope.c_select = false;
	$scope.counteries_select = null;
    $scope.counteries_select_name = "Select country";
	var selected_country = null;
	$scope.loading = false;
	$scope.validated = false;

    $scope.valid_c_select= "Select company's country";

    $scope.valid_c_set = false;
    $scope.vatC = null;

    $scope.c_touched = false;
    $scope.cc_touched = false;
    $scope.m_touched = false;

    $scope.validationMethod_name = "Select validation method";
    $scope.validationMethod = null;

    $scope.img_uploaded = false;

    $scope.imgUl = function () {
        $timeout(function() {
            $('.cloudinary-fileupload').click();
        }, 0);
    }

    $scope.loadTag = function () {
        var tag = $rootScope.img_tag;
        var elmnt = $compile(tag)($scope);
        $('.img-ul').html(elmnt);

        $('.cloudinary-fileupload').bind('fileuploadstart', function(e) {
            $('.status').html('Uploading image...');
        });
        $('.cloudinary-fileupload').bind('fileuploadfail', function(e) {
            $('.status').html("Image upload failed.");
        });

// Upload finished
        $('.cloudinary-fileupload').bind('cloudinarydone', function(e, data) {
            $('.status').html('');
            $('.preview').append(
                $.cloudinary.image(data.result.public_id,
                    {  format: data.result.format, version: data.result.version,
                        crop: 'scale', width: 200, id: 'prev', class: data.result.public_id }));
            $('.image_public_id').val(data.result.public_id);
            var current = $('#validation_imgs').val();
            $('#validation_imgs').val(data.result.public_id + "+" + current);
            angular.element('#new_prod').scope().img_uploaded = true;
            return true;
        });

    }

    $scope.setCountry = function (id, name) {
        angular.element('#country').val(id);
        $scope.counteries_select = id;
        $scope.counteries_select_name = name;
    }

    $scope.set_valid_c = function (id, name) {
        $scope.valid_c_select = name;
        $scope.valid_c_set = true;
        $scope.vatC = id;
        angular.element('#vatC').val(id);
    }

    $scope.setValidMethod = function (id, name) {
        $scope.validationMethod_name = name;
        $scope.validationMethod = id;
        angular.element('#v_method').val(id);
    }

    $scope.touched = function(i) {
        if (i == 0)
            $scope.c_touched = true;
        else if (i == 1)
            $scope.cc_touched = true;
        else if (i == 2)
            $scope.m_touched = true;
    }

	$scope.$watch('counteries_select', function() {
    	if ($scope.counteries_select == null)
    	{
    		$scope.reg_form.$setValidity("invalid", false);
    		$scope.c_select = false;
    	}
    	else
    	{
    		$scope.reg_form.$setValidity("invalid", true);
    		$scope.c_select = true;
    	}		
   	});

   	$scope.validate = function() {
   		$scope.vat_res = "";
   		if ($scope.vatC == null || $scope.vatC == undefined || $scope.vat == "")
   			$scope.vat_res = "Please fill all the data.";
   		else
   		{
   			var request = $http({
                method: "POST",
				url: "/Libraries/register_form_details.php",
                data: {
                    country: $scope.vatC,
                    vat: $scope.vat
                }
            });
            $scope.loading = true;
			request.success(function(data) {
				$scope.loading = false;
				if (data == 1)
					$scope.vat_res = "VAT is valid.";
				else
					$scope.vat_res = "VAT is not valid.";
				$scope.validated = true;
			});	
   		}
   	}

	$scope.loadC = function() {
		var request = $http({
                method: "POST",
				url: "/Libraries/location_handle.php",
                data: {
                    op: 1
                }
            });
		request.success(function(data) {
			$scope.counteries = data;
		});
	}

});