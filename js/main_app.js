var app = angular.module('app',['ngRoute', 'ui.bootstrap', 'ngMessages', 'validation.match', 'socialshare', 'ngAnimate']);

app.config(['$interpolateProvider', '$routeProvider', '$locationProvider', function ($interpolateProvider, $routeProvider, $locationProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

    $locationProvider.html5Mode(true).hashPrefix('!');

    $routeProvider.when('/', {
        templateUrl: 'Templates/html_templates/homepage.html',
        controller: 'home-controller'
    })
        .when ('/search/:query/:page_number', {
            templateUrl: 'Templates/html_templates/search_list.html',
            controller: 'search-controller'
    })
        .when ('/categories/:id/:page_number', {
            templateUrl: 'Templates/html_templates/categories.html',
            controller: 'categories-controller'
    })
        .when ('/new_product/', {
            templateUrl: 'Templates/html_templates/new_product.html',
            controller: 'new-product-controller'
    })
        .when('/inbox/', {
            templateUrl: 'Templates/html_templates/inbox.html',
            controller: 'inbox-controller'
        })
        .when('/user/:id', {
            templateUrl: 'Templates/html_templates/profile.html',
            controller: 'profile-controller'
        })
        .when('/user/:id/:page', {
            templateUrl: 'Templates/html_templates/profile.html',
            controller: 'profile-controller'
        })
        .when('/user/:id/:page/:order_id', {
            templateUrl: 'Templates/html_templates/profile.html',
            controller: 'profile-controller'
        })
        .when('/edit/profile', {
            templateUrl: 'Templates/html_templates/profile-edit.html',
            controller: 'prof-edit-controller'
        })
        .when('/edit/product/:id', {
            templateUrl: 'Templates/html_templates/update_product.html',
            controller: 'new-product-controller'
        })
        .when('/product/:id', {
            templateUrl: 'Templates/html_templates/single-product.html',
            controller: 'single_product-controller'
        })
        .when('/register/', {
            templateUrl: 'Templates/html_templates/register.html',
            controller: 'reg'
        })
        .when('/register/:error', {
            templateUrl: 'Templates/html_templates/register.html',
            controller: 'reg'
        })
        .when('/cart', {
            templateUrl: 'Templates/html_templates/cart.html',
            controller: 'cart-controller'
        })
        .when('/services', {
            templateUrl: 'Templates/html_templates/services.html',
            controller: 'services'
        })
        .when('/soon', {
            templateUrl: 'Templates/html_templates/soon.html'
        })
        .when('/terms', {
            templateUrl: 'Templates/html_templates/terms.html'
        })
        .when('/ipr', {
            templateUrl: 'Templates/html_templates/ipr-policy.html'
        })
        .when('/privacy', {
            templateUrl: 'Templates/html_templates/privacy-policy.html'
        })
        .when('/product-policy', {
            templateUrl: 'Templates/html_templates/product-listing-policy.html'
        })
        .when('/badges', {
            templateUrl: 'Templates/html_templates/badges.html'
        })
        .when('/contact', {
            templateUrl: 'Templates/html_templates/contact-us.html',
            controller: 'contact-us-controller'
        })
        .when('/payment-methods', {
            templateUrl: 'Templates/html_templates/payment-methods.html',
            controller: 'payment-methods-controller'
        })
        .otherwise ({
        redirectTo: '/'
    })

}]);

app.factory('location', [
    '$location',
    '$route',
    '$rootScope',
    function ($location, $route, $rootScope) {
        var page_route = $route.current;

        $location.skipReload = function () {
            //var lastRoute = $route.current;
            var unbind = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = page_route;
                unbind();
            });
            return $location;
        };

        if ($location.intercept) {
            throw '$location.intercept is already defined';
        }

        $location.intercept = function(url_pattern, load_url) {

            function parse_path() {
                var match = $location.path().match(url_pattern)
                if (match) {
                    match.shift();
                    return match;
                }
            }

            var unbind = $rootScope.$on("$locationChangeSuccess", function() {
                var matched = parse_path();
                if (!matched || load_url(matched) === false) {
                    return unbind();
                }
                $route.current = page_route;
            });
        };

        return $location;
    }
]);

app.controller('globalCtrl', function ($scope, $rootScope, $location)
{
    $rootScope.cloudinary = angular.element('#cloudinary').val();
    $rootScope.path = angular.element('#path').val();
    $rootScope.logedIn = angular.element('#status').val();
    $rootScope.accountType = angular.element('#acc_type').val();
    $rootScope.img_tag = document.getElementsByClassName("cloudinary-fileupload")[0];
    $rootScope.transpact_viewer = angular.element('#transpact').val();

    $scope.$on('$routeChangeSuccess', function(next, current) {
        $rootScope.current_page = $location.path();
    });
});

app.controller('services', function ($scope, $rootScope, $location, $anchorScroll, $http)
{
    $rootScope.title = "FlyBuy | Services";
    $scope.status_msg = "";
    $scope.types = ['Blog', 'Portfolio / Personal', 'E-Commerce', 'Other'];
    $scope.selected_type = $scope.types[0];

    $scope.selectType = function (index) {
        $scope.selected_type = $scope.types[index];
    }
    $(document).ready(function(){

        $header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
        $screen_height = $(window).height();
        $slider_height = $screen_height - $header_height;
        // SET HEIGHT OF SLIDER ON LOAD
        $('.slider-homepage').height($slider_height);

        // POSITIONING menu slider
        $menu_slider_pos = $slider_height - $('.search-row').height()  - 140;
        $('.slider-menu').css('margin-top', $menu_slider_pos);

        // FUNCTION TO SET SLIDER HEIGHT ON RESIZING
        $( window ).resize(function() {
            $header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
            $screen_height = $(window).height();
            $slider_height = $screen_height - $header_height;
            // SET HEIGHT OF SLIDER ON RESIZE
            $('.slider-homepage').height($slider_height);

            // SET MARGIN/POSITON of menu slider
            $menu_slider_pos = $slider_height - $('.search-row').height()  - 140;
            $('.slider-menu').css('margin-top', $menu_slider_pos);

            // SERVICES SLIDER HEIGHT
            $('.slider-content-services').height($slider_height);


        }); // END OF window.resize

        // SERVICES SLIDER HEIGHT

        $('.slider-content-services').height($slider_height);
        // SCROLL stuff services
    }); // END OF document.ready

    $scope.scrollTo = function(id) {
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
    };

    $scope.submitForm = function () {
        $scope.status_msg = "";
        var msg = "";
        msg = "<p>Name: " +$scope. con_name +
                "<br>Email: " + $scope.con_email +
                "<br>Phone/skype: " + $scope.con_skype +
                "<br>Type of a website: " + $scope.selected_type +
                "<br>Description: " + $scope.con_description +
                "<br>Budget: " + $scope.con_budget +
                "<br>Time scale: " + $scope.con_time;

        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o: 8,
                message: msg,
                replay: $scope.con_email,
                name: $scope.con_name
            }
        });
        request.success(function(data) {
            $scope.status_msg = "We have received your and a member of stuff will response withing 24h.";
        });
    }
});


