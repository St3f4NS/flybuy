app.directive('bxslider', function () {
    return {
        link: function (scope, element, attrs) {
            element.bxSlider({auto:true});
        }
    }
});

app.controller('home-controller', function($scope, $rootScope, $location, $http, $sce, $compile) {
    $rootScope.title = "FlyBuy";
    $rootScope.meta_description = "FlyBuy is a place where you can buy and sell your stuff online on a fast and secure way!";
    $rootScope.meta_keywords = "flybuy,buy,sell,online,ecommerce,trade,safe,fast,products";
    $scope.latest;
    $scope.mostSelling;
    $scope.cloudinary = $rootScope.cloudinary;
    $scope.categories = [];

    $scope.init = function() {
        $(document).ready(function() {
            $('#buy_btn').on("click", function () {
                $('.dropdown-categories-search').click();
            });

            //main_slider
            var header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
            var screen_height = $(window).height();
            var slider_height = screen_height - header_height;
            // SET HEIGHT OF SLIDER ON LOAD
            $('.slider-homepage').height(slider_height);

            // POSITIONING menu slider
            var menu_slider_pos = slider_height - $('.search-row').height() - 140;
            $('.slider-menu').css('margin-top', menu_slider_pos);

            // FUNCTION TO SET SLIDER HEIGHT ON RESIZING
            $(window).resize(function () {
                header_height = $('.header-profile-notifs').height() + $('.header-profile-wrap').height();
                screen_height = $(window).height();
                slider_height = screen_height - header_height;
                // SET HEIGHT OF SLIDER ON RESIZE
                $('.slider-homepage').height(slider_height);

                // SET MARGIN/POSITON of menu slider
                menu_slider_pos = slider_height - $('.search-row').height() - 140;
                $('.slider-menu').css('margin-top', menu_slider_pos);

            }); // END OF window.resize
        });
        $scope.show = false;
        loadCat();
        $scope.loadLatest();
        $scope.loadMostSelling();
    }

    $scope.toggleDropdown = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.show = !$scope.show;
    };

    $scope.loadLatest = function () {
            var request = $http.get("/API/latest/5");
            request.success(function(data) {
                if (data.indexOf("null") == -1)
                    $scope.latest = data;

            });
    }

    $scope.loadMostSelling = function() {
        var request = $http.get("/API/most_selling");
        request.success(function(data) {
            if (data.indexOf("null") == -1)
                $scope.mostselling = data;
        });
    }

    $scope.subscribeAdd = function() {
        $scope.subscribe_error = "";

        if ($scope.subscribe_field != "")
        {
            var request = $http({
                method: "POST",
                url: "/Libraries/subscribe_handle.php",
                data: {
                    e: $scope.subscribe_field
                }
            });
            request.success(function(data) {
                $scope.subscribe_error = data;
            })
        }
        else
            $scope.subscribe_error = "Please write your email in the text field above.";
    }
/*
    $scope.subCatLoad = function(cat, depth) {
        if (depth == 1) {
            $scope.loadingSub = true;
            $scope.category_list = null;
        }
        else {
            $scope.loadingLinks = true;
            $scope.links = null;
        }

        var req = $http({
            method: "POST",
            url: "/Libraries/categories.php",
            data: {
                op_type: 5,
                parent_id: cat
            }
        });
        req.success(function (data) {
            if (depth == 1) {
                $scope.category_list = data;
                $scope.loadingSub = false;
            }
            else {
                $scope.createLinks(data);
            }

        });
    };*/
    var loadCat = function () {
        var req = $http({
            method: "POST",
            url: "/Libraries/categories.php",
            data: {
                op_type: 6
            }
        });
        req.success(function (data) {
            $scope.categories = data;
        });
    }


    /*

     <li ng-repeat="link in links track by link.id">
     <a href="/#!/categories/{[{link.id}]}/1" ng-bind="link.name"></a>
     </li>template="{[{ childs }]}"

     body += "<ul class='dropdown-subsubcategories-home' compile='subsub'>";
     body += "<li></li>";

     body += "</ul></li>";
     */

    $scope.createLinks = function(parent, depth) {
        var body = "";
        if (depth == 1) {

            var childs = $scope.categories;
            angular.forEach(childs, function(c, a) {
                if (c.parent_id == parent) {
                    body += "<li><a href='' ng-mouseover='createLinks(" + c.id + ", 2);'>" + c.name + "</a>";
                    body += "<ul ng-if='selectedParent==" + c.id + "' class='dropdown-subsubcategories-home' compile='subsub'>";
                    body += "</ul></li>";
                }
            });
            $scope.childs = body;
            $scope.loadingSub = false;
        }
        else {
            var childs = $scope.categories;
            $scope.selectedParent = parent;

            angular.forEach(childs, function (c) {
                if (c.parent_id == parent)
                    body += "<li><a href='/#!/categories/" + c.id + "/1'>" + c.name + "</a></li>";
            });
            $scope.subsub = body;
            $scope.loadingLinks = false;
        }

    }

});

app.directive('compile', function($compile) {
    // directive factory creates a link function
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
});
