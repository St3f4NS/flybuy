function getIndexOf(arr, val, prop) {
    var l = arr.length,
        k = 0;
    for (k = 0; k < l; k = k + 1) {
        if (arr[k][prop] === val) {
            return k;
        }
    }
    return false;
}
app.directive('scrollBottom', function () {
    return {
        scope: {
            scrollBottom: "="
        },
        link: function (scope, element) {
            scope.$watchCollection('scrollBottom', function (newValue) {
                if (newValue)
                {
                    $(element).scrollTop($(element)[0].scrollHeight);
                }
            });
        }
    }
})

app.controller('inbox-controller', function($scope, $rootScope, $http, $location, socket) {
    var id = $rootScope.id;
    var c;
    var c_index;
    $scope.user2_id;
    var current_conversation;
    $scope.new_count = 0;
    $scope.selected = false;
    $rootScope.title = "FlyBuy | Inbox";

    socket.on('send:message', function (data) {

        if (current_conversation == data.message.conversation_id) {
            $scope.conversations.push(data.message);

            $scope.list[c_index].message = data.message.msg;
            var temp = $scope.list[c_index];

            $scope.list.splice(c_index,1);

            $scope.list.unshift(temp);

            c_index = 0;

            spliceNewNum(data.conversation);
        }
        else
            $scope.getList();



    });

    $scope.getList = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o: 0,
                u_id: id
            }
        });
        request.success(function(data) {
            $scope.list = data;
        });

    };

    $scope.getConversation = function(conversation, index, name) {
        if (c_index != index) {
            $scope.msg_text = "";

            var request = $http({
                method: "POST",
                url: "/Libraries/message_handle.php",
                cache: false,
                data: {
                    o: 1,
                    c_id: conversation,
                    u_id: id
                }
            });
            request.success(function (data) {
                $scope.user2_name = name;
                $scope.conversations = data;
                $scope.user2_id = data[0].user2_id;
                current_conversation = data[0].conversation_id;
                $scope.selected = true;
                if ($scope.list[index].seen == 1)
                    $scope.list[index].seen = 0;
                c = conversation;
                c_index = index;
                spliceNewNum(conversation);

            });
        }
    };

    $scope.send_msg = function() {
        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o: 2,
                c_id: c,
                u: id,
                t: $scope.msg_text
            }
        });
        request.success(function(data) {

            $scope.conversations.push(data);

            var sending_data = { u_id: $scope.user2_id, message: data, conversation_id: current_conversation };

            socket.emit('send:message', sending_data);

            $scope.list[c_index].message = data.msg;
            var temp = $scope.list[c_index];

            $scope.list.splice(c_index,1);

            $scope.list.unshift(temp);

            c_index = 0;
            $scope.msg_text = "";
        });
    };


    $scope.send_new_msg = function() {

        $scope.stat_msg = "";

        var request = $http({
            method: "POST",
            url: "/Libraries/message_handle.php",
            data: {
                o : 3,
                u2: $scope.new_msg_to,
                t: $scope.new_msg_txt,
                u: id
            }
        });

        request.success(function(data) {
            if (data == "Error while sending message." || data == "Unable to find specified user.")
                $scope.stat_msg = data;
            else
            {
                var info = $http({
                    method: "POST",
                    url: "/Libraries/message_handle.php",
                    data: {
                        o : 5,
                        u: id,
                        c: data.conversation_id
                    }
                });

                info.success(function(response) {

                    var sending_data = { u_id: response, message: data, conversation_id: -1 };
                    socket.emit('send:message', sending_data);
                    $scope.getList();
                    $scope.stat_msg = "Message sent.";
                    $scope.new_msg_to = "";
                    $scope.new_msg_txt = "";
                });
            }
        });
    };


    $scope.enter_press = function(ev)
    {
        if (ev.which === 13 && $scope.msg_text.length != 0)
            $scope.send_msg();
    }

    $scope.clear_modal = function()
    {
        $scope.new_msg_to = "";
        $scope.new_msg_txt = "";
        $scope.stat_msg = "";
    }

    var spliceNewNum = function (conversation) {
        if (current_conversation == conversation) {
            var pos = $rootScope.list_of_unique_new.indexOf(conversation);

            if (pos != -1) {
                $rootScope.list_of_unique_new.splice(pos, 1);
                $rootScope.new_count = $rootScope.list_of_unique_new.length;
            }
        }
    }

});
