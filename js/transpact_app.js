app.controller('trans-controller', function($scope, $rootScope, $routeParams, $location, $http) {
    $rootScope.title = "FlyBuy | Transpact";
    $scope.current_id = $rootScope.id;
    var trans;

    $scope.$watch('transpact', function(){
      if ($scope.transpact == "")
        $scope.m = false;
      else
        $scope.m = true;
    });

    $scope.userFetch = function() {
        var request = $http.get('/API/user/' + $scope.current_id);
        request.success(function(data) {
            trans = data['[transpact]'];
            $scope.transpact = trans;
        });
    }

    $scope.submitT = function() {
        var havet = 0;
        if (trans != undefined)
            havet = 1;
        var request = $http({
            method: "POST",
            url: "/Libraries/orders_handler.php",
            data: {
                o: 11,
                u_id: $scope.current_id,
                transemail: $scope.transpact,
                update: havet
            }
        });
        request.success(function(data){
            if (data == 1)
                $scope.status_msg = "Your information has been updated.";
            else
                $scope.status_msg = "Error adding data.";
        });
    }

})